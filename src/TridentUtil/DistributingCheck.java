/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Static methods for checks if there are any active distributions.
 * 
 * @author chrish
 */
public class DistributingCheck {

    /**
     * Checks for distributions.
     * 
     * @param distribMapIn Map object of distributing processors
     */
    public static void DistributingCheck(HashMap<String, String> distribMapIn) {
        try {
            String s;
            //ProcessBuilder pb = new ProcessBuilder("ksh", "-c", "rsh atoplcap -l atop \"ps -ef | grep auto_setup | grep -v grep\"");
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", "ssh atoplcap -l atop ps -ef | grep auto_setup | grep -v grep");
            //ProcessBuilder pb = new ProcessBuilder("ksh", "-c", "rsh cwpa113 -l atop rsh atoplcap -l atop ps -ef | grep auto_setup | grep -v grep");
            Process shell = pb.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream())); // this captures the output from the command
            while ((s = stdInput.readLine()) != null) {
                String[] items = s.split("\\s+");
                for (int i = 0; i < items.length; i++) {
                    if (items[i].endsWith(".config")) {
                        //ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", "rsh cwpa113 -l atop rsh atoplcap -l atop cat " + items[i]);
                        ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", "ssh atoplcap -l atop cat " + items[i]);
                        Process shell2 = pb2.start();
                        BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream())); // this captures the output from the command
                        String s2;
                        while ((s2 = stdInput2.readLine()) != null) {
                            if (!s2.startsWith("#") && !s2.equals("")) {
                                if (!items[0].equals("")) {
                                    distribMapIn.put(s2, items[0]);
                                } else {
                                    distribMapIn.put(s2, items[1]);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    /**
     * Rechecks the distribution status of a processor.
     * @param procIn Processor
     * @return True/False
     */
    public static boolean recheck(String procIn) {
        String cmd = "ping -c 1 -w 1" + " " + procIn + "| grep \"0%\" | awk '{print $6}'";
        byte[] dataIn = new byte[100];
        String s;

        try {
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
            Process shell = pb.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream())); // this captures the output from the command
            String output = "";
            while ((s = stdInput.readLine()) != null) { // this captures the output from the command
                output = s;
            }

            // Check output of  the ping command
            if (output.equals("100%") || output.equals("+1")) {
                return false;
            } else {

                //Run the procstate next if daemgr process is running
                String cmd1 = "ssh" + " " + procIn + " " + "-l atop \"logicals -l | grep atc_platform_state | cut -f 2 -d \"=\"\"";
                ProcessBuilder pb3 = new ProcessBuilder("ksh", "-c", cmd1);
                Process shell3 = pb3.start();
                BufferedReader stdInput3 = new BufferedReader(new InputStreamReader(shell3.getInputStream()));
                String output3 = "";
                while ((s = stdInput3.readLine()) != null) {
                    output3 = s;
                }
                if (output3.equals("loaded")) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (IOException e) {
            System.out.println("Problem gathering PCS: " + e);
        }
        return false;
    }
}
