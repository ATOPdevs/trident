/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentUtil;

import TridentGUI.CommandGUI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Command Task used in processor pop up menus.
 * 
 * @author chrish
 * @version $Revision: 1.1 $
 */
public class Command implements Runnable {

    String command;
    CommandGUI cg;
    String title;

    /**
     * Constructor.
     * 
     * @param commandIn
     * @param cgIn
     * @param titleIn
     */
    public Command(String commandIn, CommandGUI cgIn, String titleIn) {
        command = commandIn;
        cg = cgIn;
        title = titleIn;
    }

    /**
     * Run method. (Override)
     */
    @Override
    public void run() {
        ProcessBuilder pb = new ProcessBuilder("ksh", "-c", command);
        try {
            Process shell = pb.start();
            byte[] dataIn = new byte[100];
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
            String s;
            if (cg != null) {
                cg.setDefaultCloseOperation(javax.swing.JFrame.DISPOSE_ON_CLOSE);
                cg.setTitle(title);
                cg.setVisible(true);
                while ((s = stdInput.readLine()) != null) { // this captures the output from the command
                    cg.getOutput().append(s + "\n");
                    //if(s.startsWith("Unmounting") || s.contains("Unload completed")) {
                    //    cg.dispose();
                    //}
                    //cg.pcs();
                }
            } else {
                while ((s = stdInput.readLine()) != null) { // this captures the output from the command
                    System.out.println(s);
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }


    }
}
