/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentUtil;

import javax.swing.SwingWorker;

/**
 * Worker for pop up commands.
 * 
 * @author chrish
 * @version $Revision: 1.1 $
 */
public class CommandWorker extends SwingWorker {

    Command cmd;

    /**
     * Sets the command to be executed
     *
     * @param cmdIn Command to run.
     */
    public void setRunnable(Command cmdIn) {
        cmd = cmdIn;
    }

    /**
     * Executes the runnable Object
     *
     * @return Nothing.
     */
    @Override
    public Object doInBackground() {
        cmd.run();
        
        return null;
    }
}
