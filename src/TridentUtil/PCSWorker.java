/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentUtil;

import javax.swing.SwingWorker;

/**
 * Swing Worker for threaded PCSHighRes Status requests.
 *
 * @author chrish
 * @version $Revision: 1.4 $
 */
public class PCSWorker extends SwingWorker {

    PCSHighRes pcs = null;

    /**
     * Sets the runnable PCSHighRes object to be executed
     *
     * @param pcsIn The completed PCSHighRes object
     */
    public void setRunnable(PCSHighRes pcsIn) {
        pcs = pcsIn;
    }
    
    /**
     * Returns the PCS object.
     * @return PCS object.
     */
    public PCSHighRes getHighRes() {
        return pcs;
    }
    
    /**
     * Executes the runnable Object
     *
     * @return Nothing.
     */
    @Override
    public Object doInBackground() {
        if (pcs != null) {
            pcs.run();
        } else {
            return null;
        }
        if (pcs != null) {
            if (pcs.status.equals("down")) {
                pcs.run();
            }
        }
        return null;
    }
}
