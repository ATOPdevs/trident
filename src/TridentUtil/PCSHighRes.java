package TridentUtil;

/**
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import Trident.Processor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Handles getting PCSHighRes (High Resolution image) status for a Processor
 *
 * @author sysadmin
 * @version $Revision: 1.2 $
 */
public class PCSHighRes implements Runnable {

    String hostname;

    public String status;
    javax.swing.JLabel statusLabel;
    private boolean getcfno;
    Processor proc;
    boolean isBig;
    boolean isProcGuiRefresh;
    boolean localmlxstat;
    boolean localfsstat;
    boolean localmemstat;
    boolean localbsstat;

    /**
     * Constructor.
     *
     * @param hostnameIn Processor Hostname
     * @param statusLabelIn Label to update on the GUI with the correct status
     * icon
     * @param isBigIn
     * @param procGuiRefresh
     * @param mlxstat
     * @param memstat
     * @param fsstat
     * @param bsstat
     */
    public PCSHighRes(String hostnameIn, javax.swing.JLabel statusLabelIn, boolean isBigIn, boolean procGuiRefresh, boolean mlxstat, boolean memstat, boolean fsstat, boolean bsstat) {
        hostname = hostnameIn;
        statusLabel = statusLabelIn;
        getcfno = true;
        isBig = isBigIn;
        isProcGuiRefresh = procGuiRefresh;
        localmlxstat = mlxstat;
        localmemstat = memstat;
        localfsstat = fsstat;
        localbsstat = bsstat;
    }

    /**
     * Returns the mlx boolean.
     *
     * @return boolean
     */
    public boolean getMLX() {
        return localmlxstat;
    }

    public void setMLX(boolean boolIn) {
        localmlxstat = boolIn;
    }

    /**
     * Returns the fs boolean.
     *
     * @return boolean
     */
    public boolean getFS() {
        return localfsstat;
    }

    public void setFS(boolean boolIn) {
        localfsstat = boolIn;
    }

    /**
     * Returns the mem boolean.
     *
     * @return boolean
     */
    public boolean getMEM() {
        return localmemstat;
    }

    public void setMEM(boolean boolIn) {
        localmemstat = boolIn;
    }

    public boolean getBS() {
        return localbsstat;
    }
    
    public void setBS(boolean boolIn) {
        localbsstat = boolIn;
    }
    
    /**
     * Returns the label/icon object.
     *
     * @return Status Label
     */
    public javax.swing.JLabel getLabel() {
        return statusLabel;
    }

    /**
     * Indicates not to get the Processor's CFNO.
     */
    public void dontGetCfno() {
        getcfno = false;
    }

    /**
     * Returns the Processor's hostname.
     *
     * @return Hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Sets the Processor object.
     *
     * @param procIn
     */
    public void setProc(Processor procIn) {
        proc = procIn;
    }

    /**
     * Returns the Processor object.
     *
     * @return Processor object.
     */
    public Processor getProc() {
        return proc;
    }

    /**
     * Run method.
     */
    @Override
    public void run() {
        if (isProcGuiRefresh == false) {
            statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/refresh45px.png")));
        } else {
            statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/refresh150px.png")));
        }
        //String cmd = "ping -c 1 -w 3" + " " + hostname + "| grep \"0%\" | awk '{print $7}'";
        String cmd = "ping -c 1 -w 3" + " " + hostname + "| grep \"0%\" | awk '{print $6}'";
        byte[] dataIn = new byte[100];
        String s;

        try {
            //ProcessBuilder pb = new ProcessBuilder("cmd", "/C", cmd);
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
            Process shell = pb.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream())); // this captures the output from the command
            String output = "";
            while ((s = stdInput.readLine()) != null) { // this captures the output from the command
                output = s;
            }

            // Check output of  the ping command
            if (output.equals("100%") || output.startsWith("+")) {
                if (isBig) {
                    statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/reddown150px.png")));
                } else {
                    statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/reddown45px.png")));
                    statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                    statusLabel.setText("--");
                }
                status = "down";
                proc.setStatus(status);
            } else {
                if (hostname.equals("oiiflc1") || hostname.equals("tclc01") || hostname.equals("trecssim") || hostname.equals("supss301")) {
                    if (isBig) {
                        statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup150px.png")));
                    } else {
                        statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup45px.png")));
                        statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                        statusLabel.setText("--");
                    }
                } else if (hostname.startsWith("ecssims")) {
                    String cmd0 = "ssh" + " " + hostname + " " + "-l atop \"ps -ef | grep interface_connector.exe |grep -v grep\"";
                    try {
                        ProcessBuilder pb6 = new ProcessBuilder("ksh", "-c", cmd0);
                        Process shell6 = pb6.start();
                        BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell6.getInputStream()));
                        String output6 = "";

                        while ((s = stdInput2.readLine()) != null) {
                            output6 = s;
                        }

                        if (output6.equals("")) {
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup45px.png")));
                                statusLabel.setText("--");
                            }
                        } else {
                            proc.setStatus("loaded_up");
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangeup150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangeup45px.png")));
                                statusLabel.setText("--");
                            }
                        }
                        statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                        statusLabel.setText("--");
                        return;
                    } catch (IOException e) {
                        System.out.println("Problem gathering PCS: " + e);
                    }
                } else {
                    // Next check the daemgr process
                    cmd = "ssh" + " " + hostname + " " + "-l atop \"ps -ef | grep daemgr | grep -v grep\""; //Check to see if output != null
                    ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", cmd);
                    Process shell2 = pb2.start();
                    BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
                    String output2 = "";
                    while ((s = stdInput2.readLine()) != null) {
                        output2 = s;
                    }
                    // Check whether daemgr is running
                    if (!output2.contains("dae")) {
                        status = "up";
                        proc.setStatus("loaded_down");
                        if (isBig) {
                            statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangedown150px.png")));
                        } else {
                            statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangedown45px.png")));
                            statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                            statusLabel.setText("--");
                        }
                        String cmd1 = "ssh" + " " + hostname + " " + "-l atop \"logicals -l | grep atc_platform_state | cut -f 2 -d \"=\"\"";
                        ProcessBuilder pb3 = new ProcessBuilder("ksh", "-c", cmd1);
                        Process shell3 = pb3.start();
                        BufferedReader stdInput3 = new BufferedReader(new InputStreamReader(shell3.getInputStream()));
                        String output3 = "";
                        while ((s = stdInput3.readLine()) != null) {
                            output3 = s;
                        }
                        if (output3.equals("null")) {
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/null150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/null45px.png")));
                                statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                                statusLabel.setText("--");
                            }
                        }
                    } else {	//Check to see if output != null
                        //Run the procstate next if daemgr process is running
                        status = "up";
                        String cmd1 = "ssh" + " " + hostname + " " + "-l atop \"logicals -l | grep atc_platform_state | cut -f 2 -d \"=\"\"";
                        ProcessBuilder pb3 = new ProcessBuilder("ksh", "-c", cmd1);
                        Process shell3 = pb3.start();
                        BufferedReader stdInput3 = new BufferedReader(new InputStreamReader(shell3.getInputStream()));
                        String output3 = "";
                        while ((s = stdInput3.readLine()) != null) {
                            output3 = s;
                        }
                        if (output3.equals("loaded")) {
                            proc.setStatus("loaded_up");
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangeup150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangeup45px.png")));
                            }
                            if (getcfno) {
                                String cmdCfno = "ssh" + " " + hostname + " " + "-l atop \"logicals -l | grep atc_load_platform; logicals -l | grep cfno; logicals -l | grep atc_node; logicals -l | grep atc_mode\"";
                                ProcessBuilder pb4 = new ProcessBuilder("ksh", "-c", cmdCfno);
                                Process shell4 = pb4.start();
                                BufferedReader stdInput4 = new BufferedReader(new InputStreamReader(shell4.getInputStream()));
                                while ((s = stdInput4.readLine()) != null) {
                                    if (s.startsWith("atc_cfno")) {
                                        proc.setCfno(s.split("=")[1]);
                                        statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                                        statusLabel.setText(s.split("=")[1]);
                                    }
                                }
                            }
                        } else if (output3.equals("free")) {
                            proc.setStatus("free");
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup45px.png")));
                                statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                                statusLabel.setText("--");
                            }
                        } else if (output3.equals("null")) {
                            proc.setStatus("null");
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/null150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/null45px.png")));
                                statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                                statusLabel.setText("--");
                            }
                        } else {
                            proc.setStatus("unknown");
                            if (isBig) {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown150px.png")));
                            } else {
                                statusLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png")));
                                statusLabel.setFont(new java.awt.Font("Dialog", 1, 24));
                                statusLabel.setText("--");
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Problem gathering PCS: " + e);
        }
    }
}
