package TridentUtil;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author sysadmin
 */
public class motd extends JPanel {

    int x = this.getWidth(), y = 50;
    static String motdGet = "";

    public static void setMotd() {
        try {
            String cmd = "cat /home/chrish/testdir/motd";
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
            Process shell = pb.start();
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
            String s;
            while ((s = stdInput.readLine()) != null) {
                motdGet = s;
            }
        } catch (IOException ex) {
            Logger.getLogger(motd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        Font font = new Font("Monospace", Font.BOLD + Font.PLAIN, 50);
        g2.setFont(font);
        g2.setColor(Color.red);

        g2.drawString(motdGet, x, y);

        try {
            Thread.sleep(25);
        } catch (Exception ex) {
            System.out.println("Exception caught: " + ex);
        }
        x -= 5;

        double out = motdGet.length() * 3;
        if (this.getWidth() + out + x < this.getX()) {
            x = this.getWidth();
        }

        repaint();
    }
}
