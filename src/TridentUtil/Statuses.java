package TridentUtil;

import TridentGUI.TridentGUIMonitor;
import TridentGUI.ToolboxGUI;
import static TridentGUI.TridentGUIMonitor.pcsMap;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Andrew Chen
 */
public class Statuses {

    // Checks status of MLX cards on ecs and sims
    public static String mlxCheck() {
        String[] procs = {"ecsa101", "ecsa102", "ecsb601", "ecsb602",
            //"ecst301",
            "ecssims301", "ecssims302"
    //, "ecssims303"
        };
        String errorOut = "";
        int newlineCounter = 0;
        double percento;
        double d1;
        double d2;
        DecimalFormat df = new DecimalFormat("0.0");
        for (int i = 0; i < procs.length; i++) {
            ToolboxGUI.mlxLabel.setText("Checking: " + procs[i]);
            d1 = i + 1;
            d2 = procs.length;
            percento = d1 / d2;
            ToolboxGUI.mlxPercentLabel.setText(df.format(percento * 100) + "%");
            try {
                if (upChecker(procs[i]).equals("0%")) {
                    int numMlx;
                    if (!procs[i].equals("ecssims301")) {
                        numMlx = 4;
                    } else {
                        numMlx = 1;
                    }

                    String cmd = "ssh " + procs[i] + " -l root \"lsdev -C | grep mlx \" | awk '{print $1}'";
                    String cmd2 = "ssh " + procs[i] + " -l root \"lsdev -C | grep mlx \"  | awk '{print $2}'";
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
                    ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", cmd2);
                    Process shell = pb.start();
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                    String[] output = new String[numMlx];
                    String s;
                    int counter = 0;
                    while ((s = stdInput.readLine()) != null) {
                        output[counter] = s;
                        counter++;
                    }

                    Process shell2 = pb2.start();
                    BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
                    String[] output2 = new String[numMlx];
                    String s2;
                    int counter2 = 0;
                    while ((s2 = stdInput2.readLine()) != null) {
                        output2[counter2] = s2;
                        counter2++;
                    }

                    for (int a = 0; a < numMlx; a++) {
                        if (!output2[a].equals("Available") && !errorOut.contains(procs[i])) {
                            errorOut += procs[i] + " ";
                            newlineCounter++;
                            if (newlineCounter % 3 == 0) {
                                errorOut += "<br>";
                            }
                        }
                    }
                } else {
                    errorOut += procs[i] + " ";
                    newlineCounter++;
                }
            } catch (IOException ex) {
                Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ToolboxGUI.mlxPercentLabel.setText("00.0%");
        procColorUpdate("mlx", errorOut, procs);
        colorSet(procs); // change to procs

        return errorOut;
    }

    // Checks file systems for any usage over 97%
    // dd if=/dev/zero of=file.out bs=1m count=1194
    // touch -a -m -t 201512180130.09 file.out
    // -a = accessed
    // -m = modified
    // -t  = timestamp - use [[CC]YY]MMDDhhmm[.ss] time format
    public static String fsCheck() {
        String[] procs = {
            "ecsa101", "ecsa102", "drpsa101", "drpsa102", "fdpsa101", "fdpsa102", "sdpsa101", "sdpsa102", "syncsrva101", "syncsrva102",
            "mcppa101", "mcppa102", "mcppa103", "mcppa104", "sppa101", "sppa102",
            "cwpa101", "cwpa102", "cwpa103", "cwpa104", "cwpa105", "cwpa106", "cwpa107", "cwpa108", "cwpa109", "cwpa110",
            "cwpa111", "cwpa112", "cwpa113", "cwpa114", "cwpa115",
            "ecssims301", "ecssims302", "ecssims303", "supss301", "oiiflc1", "ecst301", "drpst301", "fdpst301", "sdpst301",
            "syncsrvt301", "mcppt301", "cwpt301", "cwpt302", "cwpt303", "cwpt304", "cwpt305",
            "ecsb601", "ecsb602", "drpsb601", "drpsb602", "fdpsb601", "fdpsb602", "sdpsb601", "sdpsb602", "syncsrvb601", "syncsrvb602",
            "mcppb601", "mcppb602", "mcppb603", "mcppb604", "pppb602", "sppb601", "sppb602",
            "cwpb601", "cwpb602", "cwpb603", "cwpb604", "cwpb605", "cwpb606", "cwpb607", "cwpb608", "cwpb609", "cwpb610",
            "cwpb611", "cwpb612", "cwpb613", "cwpb614"
        };
        String errorOut = "";
        int newlineCounter = 0;
        double percento;
        double d1;
        double d2;
        DecimalFormat df = new DecimalFormat("0.0");
        for (int i = 0; i < procs.length; i++) {
            ToolboxGUI.fsLabel.setText("Checking: " + procs[i]);
            d1 = i + 1;
            d2 = procs.length;
            percento = d1 / d2;
            ToolboxGUI.fsPercentLabel.setText(df.format(percento * 100) + "%");
            try {
                if (upChecker(procs[i]).equals("0%") && !TridentGUIMonitor.pcsMap.get(procs[i]).getProc().getStatus().equals("loaded_down")) {
                    String cmd = "ssh " + procs[i] + " -l atop -q df -g | grep -v Filesystem | grep -v proc | awk '{print $4}' | cut -f 1 -d %";
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
                    Process shell = pb.start();
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                    String[] output = new String[50];
                    String s;
                    int counter = 0;
                    while ((s = stdInput.readLine()) != null) {
                        output[counter] = s;
                        counter++;
                    }

                    String directoryCheck = "ssh " + procs[i] + " -q -l atop \"df -g | grep -v Filesystem | grep -v proc | awk '{print \\$7}'\"";
                    ProcessBuilder dcpb = new ProcessBuilder("ksh", "-c", directoryCheck);
                    Process dcshell = dcpb.start();
                    BufferedReader dcInput = new BufferedReader(new InputStreamReader(dcshell.getInputStream()));
                    String[] dcoutput = new String[50];
                    String dcs;
                    int dccounter = 0;
                    while ((dcs = dcInput.readLine()) != null) {
                        dcoutput[dccounter] = dcs;
                        dccounter++;
                    }

                    for (int a = 0; a < 50; a++) {
                        if (output[a] != null) {
                            if (Integer.parseInt(output[a]) >= 98) {
                                if (dcoutput[a].equals("/ocean21/debug")) {
                                    String tmprun = "ssh -q " + procs[i] + " -l root \"find /ocean21/debug -type f -mtime +3 | xargs rm\"";
                                    ProcessBuilder tmppb = new ProcessBuilder("ksh", "-c", tmprun);
                                    Process tmpshell = tmppb.start();
                                } else {
                                    errorOut += procs[i] + " ";
                                    newlineCounter++;
                                    if (newlineCounter % 3 == 0) {
                                        errorOut += "<br>";
                                    }
                                    a = 50;
                                }
                            }
                        } else {
                            a = 50;
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ToolboxGUI.fsPercentLabel.setText("00.0%");
        procColorUpdate("fs", errorOut, procs);
        colorSet(procs);
        return errorOut;
    }

    // Checks memory on processors for any issues
    public static String memCheck() {
        // no supss301 in 4th row, 3rd spot for now
//        String[] procs = {"cwpa103", "cwpa104", "cwpa107"};
        String[] procs = {
            // Channel A
            "ecsa101", "ecsa102", "drpsa101", "drpsa102", "fdpsa101", "fdpsa102", "sdpsa101", "sdpsa102", "syncsrva101", "syncsrva102",
            "mcppa101", "mcppa102", "mcppa103", "mcppa104", "sppa101", "sppa102",
            "cwpa101", "cwpa102", "cwpa103", "cwpa104", "cwpa105", "cwpa106", "cwpa107", "cwpa108", "cwpa109", "cwpa110",
            "cwpa111", "cwpa112", "cwpa113", "cwpa114", "cwpa115",
            // Channel T
            "ecssims301", "ecssims302", "ecssims302", "supss301",
            "oiiflc1", "ecst301", "drpst301", "fdpst301",
            "sdpst301", "syncsrvt301", "mcppt301", "cwpt301",
            "cwpt302", "cwpt303", "cwpt304", "cwpt305",
            // Channel B
            "ecsb601", "ecsb602", "drpsb601", "drpsb602", "fdpsb601", "fdpsb602", "sdpsb601", "sdpsb602", "syncsrvb601", "syncsrvb602",
            "mcppb601", "mcppb602", "mcppb603", "mcppb604", "pppb602", "sppb601", "sppb602",
            "cwpb601", "cwpb602", "cwpb603", "cwpb604", "cwpb605", "cwpb606", "cwpb607", "cwpb608", "cwpb609", "cwpb610",
            "cwpb611", "cwpb612", "cwpb613", "cwpb614"
        };
        String errorOut = "";
        int newlineCounter = 0;
        String single = "1696";
        String dual = "3488";
//        String dualt = "3760";
        String quad = "7712";
        String simsdual = "7456";

        // Used for percent completion display within Toolbox
        double percento;
        double d1;
        double d2;
        DecimalFormat df = new DecimalFormat("0.0");
        //

        for (int i = 0; i < procs.length; i++) {
            ToolboxGUI.memLabel.setText("Checking: " + procs[i]);
            int procCount = 0;
            String procMem = "0";
            String actualMem = "0";

            // Used for percent completion display within Toolbox
            d1 = i + 1;
            d2 = procs.length;
            percento = d1 / d2;
            ToolboxGUI.memPercentLabel.setText(df.format(percento * 100) + "%");
            //

            try {
                if (upChecker(procs[i]).equals("0%") && !TridentGUIMonitor.pcsMap.get(procs[i]).getProc().getStatus().equals("loaded_down")) {
                    String cmd;
                    if (procs[i].equals("ecssims301") || procs[i].equals("ecssims302") || procs[i].equals("ecssims303") || procs[i].equals("supss301")) {
                        cmd = "ssh " + procs[i] + " -l root lscfg | grep proc | awk '{print $2}'";
                    } else {
                        cmd = "ssh " + procs[i] + " -l atop lscfg | grep proc | awk '{print $2}'";
                    }
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
                    Process shell = pb.start();
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                    String s;

                    while ((s = stdInput.readLine()) != null) {
                        procCount++;
                    }

                    String cmd2 = "ssh " + procs[i] + " -l atop lsattr -El mem0 | grep -v Total | awk '{print $2}'";
                    ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", cmd2);
                    Process shell2 = pb2.start();
                    BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
                    String s2;

                    while ((s2 = stdInput2.readLine()) != null) {
                        actualMem = s2;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (procCount == 1) {
                procMem = single;
            } else if (procCount == 2) {
                procMem = dual;
            } else if (procCount == 4) {
                procMem = quad;
            } else {
                procMem = "NOTSURE";
            }

            if (procs[i].equals("ecssims302")) {
                procMem = simsdual;
            }

//            if (!actualMem.equals(procMem)) {
            if (!procMem.equals("NOTSURE")) {
                if (Integer.parseInt(actualMem) < Integer.parseInt(procMem)) {
                    errorOut += procs[i] + " ";
                    newlineCounter++;
                    if (newlineCounter % 3 == 0) {
                        errorOut += "<br>";
                    }
                }
            }
        }
        ToolboxGUI.memPercentLabel.setText("00.0%");
        procColorUpdate("mem", errorOut, procs);
        colorSet(procs); // set to procs
        return errorOut;
    }

    public static void bsCheck() {
        String errorOut = "";
        int newlineCounter = 0;
        String[] procs = {
            "ecsa101", "ecsa102", "drpsa101", "drpsa102", "fdpsa101", "fdpsa102", "sdpsa101", "sdpsa102", "syncsrva101", "syncsrva102",
            "mcppa101", "mcppa102", "mcppa103", "mcppa104", "sppa101", "sppa102",
            "cwpa101", "cwpa102", "cwpa103", "cwpa104", "cwpa105", "cwpa106", "cwpa107", "cwpa108", "cwpa109", "cwpa110",
            "cwpa111", "cwpa112", "cwpa113", "cwpa114", "cwpa115",
            "ecst301", "drpst301", "fdpst301", "sdpst301",
            "syncsrvt301", "mcppt301", "cwpt301", "cwpt302", "cwpt303", "cwpt304", "cwpt305",
            "ecsb601", "ecsb602", "drpsb601", "drpsb602", "fdpsb601", "fdpsb602", "sdpsb601", "sdpsb602", "syncsrvb601", "syncsrvb602",
            "mcppb601", "mcppb602", "mcppb603", "mcppb604", "pppb602", "sppb601", "sppb602",
            "cwpb601", "cwpb602", "cwpb603", "cwpb604", "cwpb605", "cwpb606", "cwpb607", "cwpb608", "cwpb609", "cwpb610",
            "cwpb611", "cwpb612", "cwpb613", "cwpb614"
        };

        double percento;
        double d1;
        double d2;
        DecimalFormat df = new DecimalFormat("0.0");
        for (int i = 0; i < procs.length; i++) {
            ToolboxGUI.bsLabel.setText("Checking: " + procs[i]);
            d1 = i + 1;
            d2 = procs.length;
            percento = d1 / d2;
            ToolboxGUI.bsPercentLabel.setText(df.format(percento * 100) + "%");
            try {
                if (upChecker(procs[i]).equals("0%") && !TridentGUIMonitor.pcsMap.get(procs[i]).getProc().getStatus().equals("loaded_down")) {

                    String cmd = "ssh oiiflc1 -l atop /tc_test/testuser/bin/pcs /tc_test/testuser/bin/pcsconfigfiles/" + procs[i] + ".config | grep -v -| grep -v processor| grep -v =| awk '{print $1, $2}'";
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
                    Process shell = pb.start();
                    BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                    String s = "";
                    String output = "";
                    while ((s = stdInput.readLine()) != null) {
                        output = s;
                    }

                    String[] elements = output.split(" ");

                    String cmd2 = "ssh oiiflc1 -l atop ssh " + procs[i] + " -l root \"ls -lrt /ocean21/conf/ocean21." + elements[1] + "/logicals\"";
                    ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", cmd2);
                    Process shell2 = pb2.start();
                    BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
                    String s2 = "";
                    String output2 = "";
                    while ((s2 = stdInput2.readLine()) != null) {
                        output2 = s2;
                    }

                    if (output2.equals("")) {
                        errorOut += procs[i] + " ";
                        newlineCounter++;
                        if (newlineCounter % 3 == 0) {
                            errorOut += "<br>";
                        }
                    }
                }

            } catch (IOException ex) {
                Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        procColorUpdate("bs", errorOut, procs);

        if (!errorOut.equals("")) {
            String[] fixArray = errorOut.split(" ");
            for (int b = 0; b < fixArray.length; b++) {
                if (!fixArray[b].equals("")) {
                    for (int g = 0; g < fixArray.length; g++) {
                        if (fixArray[g].startsWith("<br>")) {
                            fixArray[g] = fixArray[g].substring(4, fixArray[g].length());
                        }
                    }

                    String[] releaseArray = new String[10]; // stores arrays that contain release info
                    int releaseCounter = 0;
                    try {
//                        System.out.println("fixArray[" + b + "]: " + fixArray[b]);

                        String cmd3 = "ssh oiiflc1 -l atop ssh " + fixArray[b] + " -l root \"ls -lrt /ocean21/conf | grep -v relinfo | grep -v txt | grep ocean\" | awk '{print $9}'";
                        ProcessBuilder pb3 = new ProcessBuilder("ksh", "-c", cmd3);
                        Process shell3 = pb3.start();
                        BufferedReader stdInput3 = new BufferedReader(new InputStreamReader(shell3.getInputStream()));
                        String s3 = "";
                        while ((s3 = stdInput3.readLine()) != null) {
                            releaseArray[releaseCounter] = s3;
                            releaseCounter++;
                        }

                        boolean fixed = false;

                        for (int j = 0; j < releaseArray.length; j++) {
                            if (releaseArray[j] != null) {
                                String cmd4 = "ssh oiiflc1 -l atop ssh " + fixArray[b] + " -l root \"ls -lrt /ocean21/conf/" + releaseArray[j] + "/logicals\"";
                                ProcessBuilder pb4 = new ProcessBuilder("ksh", "-c", cmd4);
                                Process shell4 = pb4.start();
                                BufferedReader stdInput4 = new BufferedReader(new InputStreamReader(shell4.getInputStream()));
                                String s4 = "";
                                String output4 = "";
                                while ((s4 = stdInput4.readLine()) != null) {
                                    output4 = s4;
                                }
                                if (!output4.equals("")) {
                                    //System.out.println("VALID RELEASE FOUND: " + releaseArray[j]);
                                    //System.out.println("ATTEMPTING TO FIX!");
                                    String cmd5 = "ssh oiiflc1 -l atop ssh " + fixArray[b] + " -l root \"ln -sf /ocean21/conf/" + releaseArray[j] + " /ocean21/bootstrap\"";
                                    ProcessBuilder pb5 = new ProcessBuilder("ksh", "-c", cmd5);
                                    Process shell5 = pb5.start();
                                    fixed = true;
                                    PCSHighRes temp = TridentGUIMonitor.pcsMap.get(fixArray[b]);
                                    temp.setBS(true);
                                    j = releaseArray.length;
                                } else {
                                    System.out.println("INVALID RELEASE: " + releaseArray[j]);
                                }
                            }
                        }

                        if (fixed == false) {
                            System.out.println("Unable to link bootstrap");
                        }

                    } catch (IOException ex) {
                        Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        ToolboxGUI.bsPercentLabel.setText("00.0%");
        colorSet(procs);
    }

    public static String netCheck() {

        String errorOut = String.format("%-15s", "Processor") + String.format("%-9s", "Name") + String.format("%-20s", "Network") + String.format("%-25s", "Address") + String.format("%-8s", "Ierrs") + String.format("%-8s", "Oerrs") + String.format("%-8s", "Coll") + "\n";
        String[] procs = {
            "ecsa101", "ecsa102", "drpsa101", "drpsa102", "fdpsa101", "fdpsa102", "sdpsa101", "sdpsa102", "syncsrva101", "syncsrva102",
            "mcppa101", "mcppa102", "mcppa103", "mcppa104", "sppa101", "sppa102",
            "cwpa101", "cwpa102", "cwpa103", "cwpa104", "cwpa105", "cwpa106", "cwpa107", "cwpa108", "cwpa109", "cwpa110",
            "cwpa111", "cwpa112", "cwpa113", "cwpa114", "cwpa115",
            "ecssims301", "ecssims302", "ecssims303", "ecst301", "drpst301", "fdpst301", "sdpst301",
            "syncsrvt301", "mcppt301", "cwpt301", "cwpt302", "cwpt303", "cwpt304", "cwpt305",
            "ecsb601", "ecsb602", "drpsb601", "drpsb602", "fdpsb601", "fdpsb602", "sdpsb601", "sdpsb602", "syncsrvb601", "syncsrvb602",
            "mcppb601", "mcppb602", "mcppb603", "mcppb604", "pppb602", "sppb601", "sppb602",
            "cwpb601", "cwpb602", "cwpb603", "cwpb604", "cwpb605", "cwpb606", "cwpb607", "cwpb608", "cwpb609", "cwpb610",
            "cwpb611", "cwpb612", "cwpb613", "cwpb614",
            "oiiflc1",};

        double percento;
        double d1;
        double d2;
        DecimalFormat df = new DecimalFormat("0.0");
        for (int i = 0; i < procs.length; i++) {
            d1 = i + 1;
            d2 = procs.length;
            percento = d1 / d2;
            ToolboxGUI.netPercentLabel.setText(df.format(percento * 100) + "%");
            System.out.println(procs[i]);
            try {
                if (upChecker(procs[i]).equals("0%") && !TridentGUIMonitor.pcsMap.get(procs[i]).getProc().getStatus().equals("loaded_down")) {
                    String cmd2 = "ssh " + procs[i] + " -l atop netstat -i | grep -v lo | grep -v link | grep -v Name |grep -v warp | awk '{print $1,$3,$4,$6,$8,$9}'";
                    ProcessBuilder pb2 = new ProcessBuilder("ksh", "-c", cmd2);
                    Process shell2 = pb2.start();
                    BufferedReader stdInput2 = new BufferedReader(new InputStreamReader(shell2.getInputStream()));
                    String s2 = "";
                    String output2 = "";
                    while ((s2 = stdInput2.readLine()) != null) {
                        output2 = s2;
                        String[] elements = output2.split("\t");

                        for (int x = 0; x < elements.length; x++) {
                            elements[x] = elements[x].replaceAll("\\s+", " ");
                        }
                        String[] elements2 = elements[0].split(" ");
                        if (Integer.parseInt(elements2[3]) > 0 || Integer.parseInt(elements2[4]) > 0 || Integer.parseInt(elements2[5]) > 0) {
                            errorOut += String.format("%-15s", procs[i]) + String.format("%-9s", elements2[0]) + String.format("%-20s", elements2[1]) + String.format("%-25s", elements2[2]) + String.format("%-8s", elements2[3]) + String.format("%-8s", elements2[4]) + String.format("%-8s", elements2[5]) + "\n";
                        }
                    }
                }

            } catch (IOException ex) {
                Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ToolboxGUI.netPercentLabel.setText("00.0%");
        return errorOut;
    }

    public static void syncColShare() {
        try {
            String cmd = "ssh uxocean1 -l root ./syncColShare";
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmd);
            Process shell = pb.start();
        } catch (IOException ex) {
            Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String upChecker(String procIn) {
        String inProc = procIn;
        String retString = "";
        try {
//            System.out.println("current proc is: " + inProc);

            String cmdPing = "ping -c 1 -w 2 " + inProc + " | grep \"0%\" | awk '{print $6}'";
            ProcessBuilder pbPing = new ProcessBuilder("ksh", "-c", cmdPing);
            Process shellPing = pbPing.start();
            BufferedReader stdInputPing = new BufferedReader(new InputStreamReader(shellPing.getInputStream()));
            String sPing = "";
            String outputPing = "";
            while ((sPing = stdInputPing.readLine()) != null) {
                outputPing = sPing;
            }

//            System.out.println("outputPing for " + inProc + " is: " + outputPing);
            retString = outputPing;
        } catch (IOException ex) {
            Logger.getLogger(Statuses.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retString;
    }

    public static void colorSet(String[] procsIn) {
        String[] procArrayIn = new String[procsIn.length];
        procArrayIn = procsIn;
        for (int x = 0; x < procArrayIn.length; x++) {
            if ((pcsMap.get(procArrayIn[x]).getMLX() == true) && (pcsMap.get(procArrayIn[x]).getMEM() == true) && (pcsMap.get(procArrayIn[x]).getFS() == true) && (pcsMap.get(procArrayIn[x]).getBS() == true)) {
                pcsMap.get(procArrayIn[x]).getLabel().setForeground(Color.WHITE);
            } else {
                pcsMap.get(procArrayIn[x]).getLabel().setForeground(Color.red);
            }
        }
    }

    public static void procColorUpdate(String typeIn, String errorIn, String[] procsIn) {
        String errorString = errorIn;
        String[] procArray = new String[procsIn.length];
        procArray = procsIn;

        String[] elements = errorString.split(" ");
        if ((elements.length > 0) && !elements[0].equals("")) {
            for (int i = 0; i < elements.length; i++) {
                if (elements[i].startsWith("<br>")) {
                    elements[i] = elements[i].substring(4, elements[i].length());
                }
            }

            List<String> procList = new ArrayList<String>(Arrays.asList(procArray));
            List<String> badProcList = new ArrayList<String>();
            for (int i = 0; i < elements.length; i++) {
                if (procList.contains(elements[i])) {
                    badProcList.add(elements[i]);
                    procList.remove(elements[i]);
                }
            }

            String[] badProcListArray = new String[badProcList.size()];
            badProcListArray = badProcList.toArray(badProcListArray);

            String[] goodProcListArray = new String[procList.size()];
            goodProcListArray = procList.toArray(goodProcListArray);

            for (int i = 0; i < badProcListArray.length; i++) {
                if (typeIn.equals("mlx")) {
                    TridentGUIMonitor.pcsMap.get(badProcListArray[i]).setMLX(false);
                } else if (typeIn.equals("mem")) {
                    TridentGUIMonitor.pcsMap.get(badProcListArray[i]).setMEM(false);
                } else if (typeIn.equals("fs")) {
                    TridentGUIMonitor.pcsMap.get(badProcListArray[i]).setFS(false);
                } else if (typeIn.equals("bs")) {
                    TridentGUIMonitor.pcsMap.get(badProcListArray[i]).setBS(false);
                }
            }

            for (int i = 0; i < goodProcListArray.length; i++) {
                PCSHighRes temp = TridentGUIMonitor.pcsMap.get(goodProcListArray[i]);
                if (typeIn.equals("mlx")) {
                    TridentGUIMonitor.pcsMap.get(goodProcListArray[i]).setMLX(true);
                } else if (typeIn.equals("mem")) {
                    TridentGUIMonitor.pcsMap.get(goodProcListArray[i]).setMEM(true);
                } else if (typeIn.equals("fs")) {
                    TridentGUIMonitor.pcsMap.get(goodProcListArray[i]).setFS(true);
                } else if (typeIn.equals("bs")) {
                    TridentGUIMonitor.pcsMap.get(goodProcListArray[i]).setBS(true);
                }
            }
        }
    }
}
