package TridentGUI;

import Trident.Processor;
import TridentUtil.motd;
import TridentUtil.DistributingCheck;
import TridentUtil.PCSHighRes;
import TridentUtil.PCSWorker;
import TridentUtil.Statuses;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.SwingWorker;

/**
 * GUI for Processor status monitoring.
 *
 * @author chrish
 */
public class TridentGUIMonitor extends javax.swing.JFrame {

    private ArrayList<PCSWorker> workers;
    private HashMap<String, String> distribMap;
    public static HashMap<String, PCSHighRes> pcsMap;
    private ArrayList<Processor> processorList;
    public static String mlxout;
    public static String memout;
    public static String fsout;
    public static String synccolout;
    public static String bsout;

    /**
     * Creates new form TridentGUIMonitor
     */
    public TridentGUIMonitor() {
        initComponents();
        addMouseListeners();
        Timer timer = new Timer(); // 60 second refresh
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                workers = new ArrayList<PCSWorker>();
                setPCS();
                startPolling();
                motdPanel = new motd();
                motd.setMotd();
            }
        }, 0, 60 * 1000); // once a minute

        Timer dayTimer = new Timer(); // 24 hour refresh
        dayTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                SwingWorker worker;
                worker = new SwingWorker() {
                    Date dNow = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("E hh:mm:ss a zzz");

                    @Override
                    protected Object doInBackground() {
                        // Runs mlx check
                        String out = Statuses.mlxCheck();
                        if (out.equals("")) {
                            mlxout = "OK @ " + ft.format(dNow);
                        } else {
                            mlxout = "<html>MLX issues on:<br> " + out + "<br>Checked @ " + ft.format(dNow) + "</html>";
                        }

                        // Runs memory check
                        out = Statuses.memCheck();
                        if (out.equals("")) {
                            memout = "OK @ " + ft.format(dNow);
                        } else {
                            memout = "<html>Memory issues on:<br> " + out + "<br>Checked @ " + ft.format(dNow) + "</html>";
                        }
                        return null;
                    }
                };
                worker.execute();
            }
        }, 0, 86400 * 1000); // once a day

        Timer hourTimer = new Timer(); // 60 minute refresh
        hourTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                SwingWorker worker;
                String temp;
                worker = new SwingWorker() {
                    Date dNow = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("E hh:mm:ss a zzz");

                    @Override
                    protected Object doInBackground() {
                        // Runs filesystem check
                        String out = Statuses.fsCheck();
                        if (out.equals("")) {
                            fsout = "OK @ " + ft.format(dNow);
                        } else {
                            fsout = "<html>Filesystem issues on:<br>" + out + "<br>Checked @ " + ft.format(dNow) + "</html>";
                        }

                        // Updates uxocean with latest auto files from /Colossus_Share/colossus on lab controller
                        Statuses.syncColShare();
                        synccolout = "Updated " + ft.format(dNow);
                        return null;

                        /*String out = Statuses.bsCheck();
                         if (out.equals("")) {
                         bsLabel.setText("OK @ " + ft.format(dNow));
                         } else {
                         bsLabel.setText("<html>Bootstrap issues on:<br>" + out + "<br>Checked @ " + ft.format(dNow) + "</html>");
                         }*/
                    }
                };
                worker.execute();
            }
        }, 0, 3600 * 1000); // check every hour
    }

    /**
     * Adds MouseListeners to icons.
     */
    private void addMouseListeners() {
        ecsa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecsa101").equals("0%")) {
                    ecsa101MouseClicked(evt);
                }
            }
        });
        ecsa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecsa102").equals("0%")) {
                    ecsa102MouseClicked(evt);
                }
            }
        });
        drpsa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("drpsa101").equals("0%")) {
                    drpsa101MouseClicked(evt);
                }
            }
        });
        drpsa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("drpsa102").equals("0%")) {
                    drpsa102MouseClicked(evt);
                }
            }
        });
        fdpsa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("fdpsa101").equals("0%")) {
                    fdpsa101MouseClicked(evt);
                }
            }
        });
        fdpsa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("fdpsa102").equals("0%")) {
                    fdpsa102MouseClicked(evt);
                }
            }
        });
        sdpsa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sdpsa101").equals("0%")) {
                    sdpsa101MouseClicked(evt);
                }
            }
        });
        sdpsa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sdpsa102").equals("0%")) {
                    sdpsa102MouseClicked(evt);
                }
            }
        });
        syncsrva101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("syncsrva101").equals("0%")) {
                    syncsrva101MouseClicked(evt);
                }
            }
        });
        syncsrva102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("syncsrva102").equals("0%")) {
                    syncsrva102MouseClicked(evt);
                }
            }
        });
        mcppa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppa101").equals("0%")) {
                    mcppa101MouseClicked(evt);
                }
            }
        });
        mcppa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppa102").equals("0%")) {
                    mcppa102MouseClicked(evt);
                }
            }
        });
        mcppa103.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppa103").equals("0%")) {
                    mcppa103MouseClicked(evt);
                }
            }
        });
        mcppa104.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppa104").equals("0%")) {
                    mcppa104MouseClicked(evt);
                }
            }
        });
        sppa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sppa101").equals("0%")) {
                    sppa101MouseClicked(evt);
                }
            }
        });
        sppa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sppa102").equals("0%")) {
                    sppa102MouseClicked(evt);
                }
            }
        });
        cwpa101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa101").equals("0%")) {
                    cwpa101MouseClicked(evt);
                }
            }
        });
        cwpa102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa102").equals("0%")) {
                    cwpa102MouseClicked(evt);
                }
            }
        });
        cwpa103.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa103").equals("0%")) {
                    cwpa103MouseClicked(evt);
                }
            }
        });
        cwpa104.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa104").equals("0%")) {
                    cwpa104MouseClicked(evt);
                }
            }
        });
        cwpa105.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa105").equals("0%")) {
                    cwpa105MouseClicked(evt);
                }
            }
        });
        cwpa106.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa106").equals("0%")) {
                    cwpa106MouseClicked(evt);
                }
            }
        });
        cwpa107.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa107").equals("0%")) {
                    cwpa107MouseClicked(evt);
                }
            }
        });
        cwpa108.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa108").equals("0%")) {
                    cwpa108MouseClicked(evt);
                }
            }
        });
        cwpa109.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa109").equals("0%")) {
                    cwpa109MouseClicked(evt);
                }
            }
        });
        cwpa110.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa110").equals("0%")) {
                    cwpa110MouseClicked(evt);
                }
            }
        });
        cwpa111.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa111").equals("0%")) {
                    cwpa111MouseClicked(evt);
                }
            }
        });
        cwpa112.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa112").equals("0%")) {
                    cwpa112MouseClicked(evt);
                }
            }
        });
        cwpa113.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa113").equals("0%")) {
                    cwpa113MouseClicked(evt);
                }
            }
        });
        cwpa114.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa114").equals("0%")) {
                    cwpa114MouseClicked(evt);
                }
            }
        });
        cwpa115.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpa115").equals("0%")) {
                    cwpa115MouseClicked(evt);
                }
            }
        });
        ecsb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecsb601").equals("0%")) {
                    ecsb601MouseClicked(evt);
                }
            }
        });
        ecsb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecsb602").equals("0%")) {
                    ecsb602MouseClicked(evt);
                }
            }
        });
        drpsb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("drpsb601").equals("0%")) {
                    drpsb601MouseClicked(evt);
                }
            }
        });
        drpsb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("drpsb602").equals("0%")) {
                    drpsb602MouseClicked(evt);
                }
            }
        });
        fdpsb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("fdpsb601").equals("0%")) {
                    fdpsb601MouseClicked(evt);
                }
            }
        });
        fdpsb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("fdpsb602").equals("0%")) {
                    fdpsb602MouseClicked(evt);
                }
            }
        });
        sdpsb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sdpsb601").equals("0%")) {
                    sdpsb601MouseClicked(evt);
                }
            }
        });
        sdpsb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sdpsb602").equals("0%")) {
                    sdpsb602MouseClicked(evt);
                }
            }
        });
        syncsrvb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("syncsrvb601").equals("0%")) {
                    syncsrvb601MouseClicked(evt);
                }
            }
        });
        syncsrvb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("syncsrvb602").equals("0%")) {
                    syncsrvb602MouseClicked(evt);
                }
            }
        });
        mcppb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppb601").equals("0%")) {
                    mcppb601MouseClicked(evt);
                }
            }
        });
        mcppb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppb602").equals("0%")) {
                    mcppb602MouseClicked(evt);
                }
            }
        });
        mcppb603.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppb603").equals("0%")) {
                    mcppb603MouseClicked(evt);
                }
            }
        });
        mcppb604.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppb604").equals("0%")) {
                    mcppb604MouseClicked(evt);
                }
            }
        });
        sppb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sppb601").equals("0%")) {
                    sppb601MouseClicked(evt);
                }
            }
        });
        sppb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sppb602").equals("0%")) {
                    sppb602MouseClicked(evt);
                }
            }
        });
        cwpb614.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("pppb601").equals("0%")) {
                    cwpb614MouseClicked(evt);
                }
            }
        });
        cwpb601.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb601").equals("0%")) {
                    cwpb601MouseClicked(evt);
                }
            }
        });
        cwpb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb602").equals("0%")) {
                    cwpb602MouseClicked(evt);
                }
            }
        });
        cwpb603.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb603").equals("0%")) {
                    cwpb603MouseClicked(evt);
                }
            }
        });
        cwpb604.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb604").equals("0%")) {
                    cwpb604MouseClicked(evt);
                }
            }
        });
        cwpb605.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb605").equals("0%")) {
                    cwpb605MouseClicked(evt);
                }
            }
        });
        cwpb606.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb606").equals("0%")) {
                    cwpb606MouseClicked(evt);
                }
            }
        });
        cwpb607.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb607").equals("0%")) {
                    cwpb607MouseClicked(evt);
                }
            }
        });
        cwpb608.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb608").equals("0%")) {
                    cwpb608MouseClicked(evt);
                }
            }
        });
        cwpb609.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb609").equals("0%")) {
                    cwpb609MouseClicked(evt);
                }
            }
        });
        cwpb610.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb610").equals("0%")) {
                    cwpb610MouseClicked(evt);
                }
            }
        });
        cwpb611.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb611").equals("0%")) {
                    cwpb611MouseClicked(evt);
                }
            }
        });
        cwpb612.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb612").equals("0%")) {
                    cwpb612MouseClicked(evt);
                }
            }
        });
        cwpb613.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb613").equals("0%")) {
                    cwpb613MouseClicked(evt);
                }
            }
        });
        cwpb614.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpb614").equals("0%")) {
                    cwpb614MouseClicked(evt);
                }
            }
        });
        pppb602.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("pppb602").equals("0%")) {
                    pppb602MouseClicked(evt);
                }
            }
        });
        ecssims303.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecssims303").equals("0%")) {
                    ecssims303MouseClicked(evt);
                }
            }
        });
        supss301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                supss301MouseClicked(evt);
            }
        });
        oiiflc1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                oiiflc1MouseClicked(evt);
            }
        });
        cwpt301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpt301").equals("0%")) {
                    cwpt301MouseClicked(evt);
                }
            }
        });
        cwpt302.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpt302").equals("0%")) {
                    cwpt302MouseClicked(evt);
                }
            }
        });
        cwpt303.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpt303").equals("0%")) {
                    cwpt303MouseClicked(evt);
                }
            }
        });
        cwpt304.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpt304").equals("0%")) {
                    cwpt304MouseClicked(evt);
                }
            }
        });
        cwpt305.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("cwpt305").equals("0%")) {
                    cwpt305MouseClicked(evt);
                }
            }
        });
        ecst301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("ecst301").equals("0%")) {
                    ecst301MouseClicked(evt);
                }
            }
        });
        drpst301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("drpst301").equals("0%")) {
                    drpst301MouseClicked(evt);
                }
            }
        });
        fdpst301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("fdpst301").equals("0%")) {
                    fdpst301MouseClicked(evt);
                }
            }
        });
        sdpst301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("sdpst301").equals("0%")) {
                    sdpst301MouseClicked(evt);
                }
            }
        });
        syncsrvt301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("syncsrvt301").equals("0%")) {
                    syncsrvt301MouseClicked(evt);
                }
            }
        });
        mcppt301.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (Statuses.upChecker("mcppt301").equals("0%")) {
                    mcppt301MouseClicked(evt);
                }
            }
        });
    }

    /**
     * Sets the PCS objects.
     */
    private void setPCS() {
        pcsMap = new HashMap<String, PCSHighRes>();
        // Channel A
        pcsMap.put("ecsa101", new PCSHighRes("ecsa101", this.ecsa101, false, false, true, true, true, true));
        pcsMap.put("ecsa102", new PCSHighRes("ecsa102", this.ecsa102, false, false, true, true, true, true));
        pcsMap.put("drpsa101", new PCSHighRes("drpsa101", this.drpsa101, false, false, true, true, true, true));
        pcsMap.put("drpsa102", new PCSHighRes("drpsa102", this.drpsa102, false, false, true, true, true, true));
        pcsMap.put("fdpsa101", new PCSHighRes("fdpsa101", this.fdpsa101, false, false, true, true, true, true));
        pcsMap.put("fdpsa102", new PCSHighRes("fdpsa102", this.fdpsa102, false, false, true, true, true, true));
        pcsMap.put("sdpsa101", new PCSHighRes("sdpsa101", this.sdpsa101, false, false, true, true, true, true));
        pcsMap.put("sdpsa102", new PCSHighRes("sdpsa102", this.sdpsa102, false, false, true, true, true, true));
        pcsMap.put("syncsrva101", new PCSHighRes("syncsrva101", this.syncsrva101, false, false, true, true, true, true));
        pcsMap.put("syncsrva102", new PCSHighRes("syncsrva102", this.syncsrva102, false, false, true, true, true, true));
        pcsMap.put("mcppa101", new PCSHighRes("mcppa101", this.mcppa101, false, false, true, true, true, true));
        pcsMap.put("mcppa102", new PCSHighRes("mcppa102", this.mcppa102, false, false, true, true, true, true));
        pcsMap.put("mcppa103", new PCSHighRes("mcppa103", this.mcppa103, false, false, true, true, true, true));
        pcsMap.put("mcppa104", new PCSHighRes("mcppa104", this.mcppa104, false, false, true, true, true, true));
        pcsMap.put("sppa101", new PCSHighRes("sppa101", this.sppa101, false, false, true, true, true, true));
        pcsMap.put("sppa102", new PCSHighRes("sppa102", this.sppa102, false, false, true, true, true, true));
        pcsMap.put("cwpa101", new PCSHighRes("cwpa101", this.cwpa101, false, false, true, true, true, true));
        pcsMap.put("cwpa102", new PCSHighRes("cwpa102", this.cwpa102, false, false, true, true, true, true));
        pcsMap.put("cwpa103", new PCSHighRes("cwpa103", this.cwpa103, false, false, true, true, true, true));
        pcsMap.put("cwpa104", new PCSHighRes("cwpa104", this.cwpa104, false, false, true, true, true, true));
        pcsMap.put("cwpa105", new PCSHighRes("cwpa105", this.cwpa105, false, false, true, true, true, true));
        pcsMap.put("cwpa106", new PCSHighRes("cwpa106", this.cwpa106, false, false, true, true, true, true));
        pcsMap.put("cwpa107", new PCSHighRes("cwpa107", this.cwpa107, false, false, true, true, true, true));
        pcsMap.put("cwpa108", new PCSHighRes("cwpa108", this.cwpa108, false, false, true, true, true, true));
        pcsMap.put("cwpa109", new PCSHighRes("cwpa109", this.cwpa109, false, false, true, true, true, true));
        pcsMap.put("cwpa110", new PCSHighRes("cwpa110", this.cwpa110, false, false, true, true, true, true));
        pcsMap.put("cwpa111", new PCSHighRes("cwpa111", this.cwpa111, false, false, true, true, true, true));
        pcsMap.put("cwpa112", new PCSHighRes("cwpa112", this.cwpa112, false, false, true, true, true, true));
        pcsMap.put("cwpa113", new PCSHighRes("cwpa113", this.cwpa113, false, false, true, true, true, true));
        pcsMap.put("cwpa114", new PCSHighRes("cwpa114", this.cwpa114, false, false, true, true, true, true));
        pcsMap.put("cwpa115", new PCSHighRes("cwpa115", this.cwpa115, false, false, true, true, true, true));
        // Channel B
        pcsMap.put("ecsb601", new PCSHighRes("ecsb601", this.ecsb601, false, false, true, true, true, true));
        pcsMap.put("ecsb602", new PCSHighRes("ecsb602", this.ecsb602, false, false, true, true, true, true));
        pcsMap.put("drpsb601", new PCSHighRes("drpsb601", this.drpsb601, false, false, true, true, true, true));
        pcsMap.put("drpsb602", new PCSHighRes("drpsb602", this.drpsb602, false, false, true, true, true, true));
        pcsMap.put("fdpsb601", new PCSHighRes("fdpsb601", this.fdpsb601, false, false, true, true, true, true));
        pcsMap.put("fdpsb602", new PCSHighRes("fdpsb602", this.fdpsb602, false, false, true, true, true, true));
        pcsMap.put("sdpsb601", new PCSHighRes("sdpsb601", this.sdpsb601, false, false, true, true, true, true));
        pcsMap.put("sdpsb602", new PCSHighRes("sdpsb602", this.sdpsb602, false, false, true, true, true, true));
        pcsMap.put("syncsrvb601", new PCSHighRes("syncsrvb601", this.syncsrvb601, false, false, true, true, true, true));
        pcsMap.put("syncsrvb602", new PCSHighRes("syncsrvb602", this.syncsrvb602, false, false, true, true, true, true));
        pcsMap.put("mcppb601", new PCSHighRes("mcppb601", this.mcppb601, false, false, true, true, true, true));
        pcsMap.put("mcppb602", new PCSHighRes("mcppb602", this.mcppb602, false, false, true, true, true, true));
        pcsMap.put("mcppb603", new PCSHighRes("mcppb603", this.mcppb603, false, false, true, true, true, true));
        pcsMap.put("mcppb604", new PCSHighRes("mcppb604", this.mcppb604, false, false, true, true, true, true));
        pcsMap.put("pppb602", new PCSHighRes("pppb602", this.pppb602, false, false, true, true, true, true));
        pcsMap.put("sppb601", new PCSHighRes("sppb601", this.sppb601, false, false, true, true, true, true));
        pcsMap.put("sppb602", new PCSHighRes("sppb602", this.sppb602, false, false, true, true, true, true));
        pcsMap.put("cwpb601", new PCSHighRes("cwpb601", this.cwpb601, false, false, true, true, true, true));
        pcsMap.put("cwpb602", new PCSHighRes("cwpb602", this.cwpb602, false, false, true, true, true, true));
        pcsMap.put("cwpb603", new PCSHighRes("cwpb603", this.cwpb603, false, false, true, true, true, true));
        pcsMap.put("cwpb604", new PCSHighRes("cwpb604", this.cwpb604, false, false, true, true, true, true));
        pcsMap.put("cwpb605", new PCSHighRes("cwpb605", this.cwpb605, false, false, true, true, true, true));
        pcsMap.put("cwpb606", new PCSHighRes("cwpb606", this.cwpb606, false, false, true, true, true, true));
        pcsMap.put("cwpb607", new PCSHighRes("cwpb607", this.cwpb607, false, false, true, true, true, true));
        pcsMap.put("cwpb608", new PCSHighRes("cwpb608", this.cwpb608, false, false, true, true, true, true));
        pcsMap.put("cwpb609", new PCSHighRes("cwpb609", this.cwpb609, false, false, true, true, true, true));
        pcsMap.put("cwpb610", new PCSHighRes("cwpb610", this.cwpb610, false, false, true, true, true, true));
        pcsMap.put("cwpb611", new PCSHighRes("cwpb611", this.cwpb611, false, false, true, true, true, true));
        pcsMap.put("cwpb612", new PCSHighRes("cwpb612", this.cwpb612, false, false, true, true, true, true));
        pcsMap.put("cwpb612", new PCSHighRes("cwpb612", this.cwpb612, false, false, true, true, true, true));
        pcsMap.put("cwpb613", new PCSHighRes("cwpb613", this.cwpb613, false, false, true, true, true, true));
        pcsMap.put("cwpb614", new PCSHighRes("cwpb614", this.cwpb614, false, false, true, true, true, true));
        // Support
        pcsMap.put("ecssims301", new PCSHighRes("ecssims301", this.ecssims301, false, false, true, true, true, true));
        pcsMap.put("ecssims302", new PCSHighRes("ecssims302", this.ecssims302, false, false, true, true, true, true));
        pcsMap.put("supss301", new PCSHighRes("supss301", this.supss301, false, false, true, true, true, true));
        pcsMap.put("oiiflc1", new PCSHighRes("oiiflc1", this.oiiflc1, false, false, true, true, true, true));
        pcsMap.put("cwpt301", new PCSHighRes("cwpt301", this.cwpt301, false, false, true, true, true, true));
        pcsMap.put("cwpt302", new PCSHighRes("cwpt302", this.cwpt302, false, false, true, true, true, true));
        pcsMap.put("cwpt303", new PCSHighRes("cwpt303", this.cwpt303, false, false, true, true, true, true));
        pcsMap.put("cwpt304", new PCSHighRes("cwpt304", this.cwpt304, false, false, true, true, true, true));
        pcsMap.put("cwpt305", new PCSHighRes("cwpt305", this.cwpt305, false, false, true, true, true, true));
        pcsMap.put("ecst301", new PCSHighRes("ecst301", this.ecst301, false, false, true, true, true, true));
        pcsMap.put("drpst301", new PCSHighRes("drpst301", this.drpst301, false, false, true, true, true, true));
        pcsMap.put("fdpst301", new PCSHighRes("fdpst301", this.fdpst301, false, false, true, true, true, true));
        pcsMap.put("sdpst301", new PCSHighRes("sdpst301", this.sdpst301, false, false, true, true, true, true));
        pcsMap.put("syncsrvt301", new PCSHighRes("syncsrvt301", this.syncsrvt301, false, false, true, true, true, true));
        pcsMap.put("mcppt301", new PCSHighRes("mcppt301", this.mcppt301, false, false, true, true, true, true));
        pcsMap.put("ecssims303", new PCSHighRes("ecssims303", this.ecssims303, false, false, true, true, true, true));

        processorList = new ArrayList<Processor>();
        Object[] keySet = pcsMap.keySet().toArray();
        for (int i = 0; i < keySet.length; i++) {
            Processor proc = new Processor((String) keySet[i]);
            processorList.add(proc);
            pcsMap.get((String) keySet[i]).setProc(proc);
            PCSWorker worker = new PCSWorker();
            worker.setRunnable(pcsMap.get((String) keySet[i]));
            workers.add(worker);
        }
    }

    /**
     * Begins polling the PCS statuses.
     */
    private void startPolling() {
        try {
            distribMap = new HashMap<String, String>();
            DistributingCheck.DistributingCheck(distribMap);
            for (int i = 0; i < workers.size(); i++) {
                if (!distribMap.containsKey(workers.get(i).getHighRes().getHostname())) {
                    workers.get(i).execute();
                } else {
                    if (DistributingCheck.recheck(workers.get(i).getHighRes().getHostname())) {
                        workers.get(i).getHighRes().getProc().setDistrib(false);
                        workers.get(i).getHighRes().getProc().setDistUser("");
                        workers.get(i).execute();
                    } else {
                        workers.get(i).getHighRes().getLabel().setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/dist45px.png")));
                        workers.get(i).getHighRes().getLabel().setFont(new java.awt.Font("Dialog", 1, 10));
                        workers.get(i).getHighRes().getLabel().setText(distribMap.get(workers.get(i).getHighRes().getHostname()).substring(0, 4).toUpperCase());
                        workers.get(i).getHighRes().getProc().setDistrib(true);
                        workers.get(i).getHighRes().getProc().setDistUser(distribMap.get(workers.get(i).getHighRes().getHostname()).toUpperCase());
                    }
                }

            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        MainPanel = new javax.swing.JPanel();
        ChannelA = new javax.swing.JPanel();
        ecsa101Panel = new javax.swing.JPanel();
        ecsa101 = new javax.swing.JLabel();
        ecsa102Panel = new javax.swing.JPanel();
        ecsa102 = new javax.swing.JLabel();
        mcppa101Panel = new javax.swing.JPanel();
        mcppa101 = new javax.swing.JLabel();
        mcppa102Panel = new javax.swing.JPanel();
        mcppa102 = new javax.swing.JLabel();
        drpsa101Panel = new javax.swing.JPanel();
        drpsa101 = new javax.swing.JLabel();
        mcppa103Panel = new javax.swing.JPanel();
        mcppa103 = new javax.swing.JLabel();
        drpsa102Panel = new javax.swing.JPanel();
        drpsa102 = new javax.swing.JLabel();
        mcppa104Panel = new javax.swing.JPanel();
        mcppa104 = new javax.swing.JLabel();
        fdpsa101Panel = new javax.swing.JPanel();
        fdpsa101 = new javax.swing.JLabel();
        cwpa101Panel = new javax.swing.JPanel();
        cwpa101 = new javax.swing.JLabel();
        fdpsa102Panel = new javax.swing.JPanel();
        fdpsa102 = new javax.swing.JLabel();
        cwpa102Panel = new javax.swing.JPanel();
        cwpa102 = new javax.swing.JLabel();
        sdpsa101Panel = new javax.swing.JPanel();
        sdpsa101 = new javax.swing.JLabel();
        cwpa103Panel = new javax.swing.JPanel();
        cwpa103 = new javax.swing.JLabel();
        sdpsa102Panel = new javax.swing.JPanel();
        sdpsa102 = new javax.swing.JLabel();
        cwpa104Panel = new javax.swing.JPanel();
        cwpa104 = new javax.swing.JLabel();
        sncsa101Panel = new javax.swing.JPanel();
        syncsrva101 = new javax.swing.JLabel();
        cwpa105Panel = new javax.swing.JPanel();
        cwpa105 = new javax.swing.JLabel();
        sncsa102Panel = new javax.swing.JPanel();
        syncsrva102 = new javax.swing.JLabel();
        cwpa106Panel = new javax.swing.JPanel();
        cwpa106 = new javax.swing.JLabel();
        cwpa107Panel = new javax.swing.JPanel();
        cwpa107 = new javax.swing.JLabel();
        cwpa108Panel = new javax.swing.JPanel();
        cwpa108 = new javax.swing.JLabel();
        cwpa114Panel = new javax.swing.JPanel();
        cwpa114 = new javax.swing.JLabel();
        cwpa109Panel = new javax.swing.JPanel();
        cwpa109 = new javax.swing.JLabel();
        cwpa115Panel = new javax.swing.JPanel();
        cwpa115 = new javax.swing.JLabel();
        cwpa110Panel = new javax.swing.JPanel();
        cwpa110 = new javax.swing.JLabel();
        sppa101Panel = new javax.swing.JPanel();
        sppa101 = new javax.swing.JLabel();
        cwpa111Panel = new javax.swing.JPanel();
        cwpa111 = new javax.swing.JLabel();
        sppa102Panel = new javax.swing.JPanel();
        sppa102 = new javax.swing.JLabel();
        cwpa112Panel = new javax.swing.JPanel();
        cwpa112 = new javax.swing.JLabel();
        cwpa113Panel = new javax.swing.JPanel();
        cwpa113 = new javax.swing.JLabel();
        ChannelB = new javax.swing.JPanel();
        ecsb601Panel = new javax.swing.JPanel();
        ecsb601 = new javax.swing.JLabel();
        ecsb602Panel = new javax.swing.JPanel();
        ecsb602 = new javax.swing.JLabel();
        mcppb601Panel = new javax.swing.JPanel();
        mcppb601 = new javax.swing.JLabel();
        mcppb602Panel = new javax.swing.JPanel();
        mcppb602 = new javax.swing.JLabel();
        drpsb601Panel = new javax.swing.JPanel();
        drpsb601 = new javax.swing.JLabel();
        mcppb603Panel = new javax.swing.JPanel();
        mcppb603 = new javax.swing.JLabel();
        drpsb602Panel = new javax.swing.JPanel();
        drpsb602 = new javax.swing.JLabel();
        mcppb604Panel = new javax.swing.JPanel();
        mcppb604 = new javax.swing.JLabel();
        fdpsb601Panel = new javax.swing.JPanel();
        fdpsb601 = new javax.swing.JLabel();
        cwpb601Panel = new javax.swing.JPanel();
        cwpb601 = new javax.swing.JLabel();
        fdpsb602Panel = new javax.swing.JPanel();
        fdpsb602 = new javax.swing.JLabel();
        cwpb602Panel = new javax.swing.JPanel();
        cwpb602 = new javax.swing.JLabel();
        sdpsb601Panel = new javax.swing.JPanel();
        sdpsb601 = new javax.swing.JLabel();
        cwpb603Panel = new javax.swing.JPanel();
        cwpb603 = new javax.swing.JLabel();
        sdpsb602Panel = new javax.swing.JPanel();
        sdpsb602 = new javax.swing.JLabel();
        cwpb604Panel = new javax.swing.JPanel();
        cwpb604 = new javax.swing.JLabel();
        sncsb601Panel = new javax.swing.JPanel();
        syncsrvb601 = new javax.swing.JLabel();
        cwpb605Panel = new javax.swing.JPanel();
        cwpb605 = new javax.swing.JLabel();
        sncsb602Panel = new javax.swing.JPanel();
        syncsrvb602 = new javax.swing.JLabel();
        cwpb606Panel = new javax.swing.JPanel();
        cwpb606 = new javax.swing.JLabel();
        cwpb607Panel = new javax.swing.JPanel();
        cwpb607 = new javax.swing.JLabel();
        cwpb608Panel = new javax.swing.JPanel();
        cwpb608 = new javax.swing.JLabel();
        cwpb614Panel = new javax.swing.JPanel();
        cwpb614 = new javax.swing.JLabel();
        cwpb609Panel = new javax.swing.JPanel();
        cwpb609 = new javax.swing.JLabel();
        pppb602Panel = new javax.swing.JPanel();
        pppb602 = new javax.swing.JLabel();
        cwpb610Panel = new javax.swing.JPanel();
        cwpb610 = new javax.swing.JLabel();
        sppb601Panel = new javax.swing.JPanel();
        sppb601 = new javax.swing.JLabel();
        sppb602Panel = new javax.swing.JPanel();
        sppb602 = new javax.swing.JLabel();
        cwpb611Panel = new javax.swing.JPanel();
        cwpb611 = new javax.swing.JLabel();
        cwpb612Panel = new javax.swing.JPanel();
        cwpb612 = new javax.swing.JLabel();
        cwpb613Panel = new javax.swing.JPanel();
        cwpb613 = new javax.swing.JLabel();
        Legend = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        SupportS = new javax.swing.JPanel();
        sims301Panel = new javax.swing.JPanel();
        ecssims301 = new javax.swing.JLabel();
        sims302Panel = new javax.swing.JPanel();
        ecssims302 = new javax.swing.JLabel();
        supssPanel = new javax.swing.JPanel();
        supss301 = new javax.swing.JLabel();
        oiiflc1Panel = new javax.swing.JPanel();
        oiiflc1 = new javax.swing.JLabel();
        ecst301Panel = new javax.swing.JPanel();
        ecst301 = new javax.swing.JLabel();
        drpst301Panel = new javax.swing.JPanel();
        drpst301 = new javax.swing.JLabel();
        fdpst301Panel = new javax.swing.JPanel();
        fdpst301 = new javax.swing.JLabel();
        sdpst301Panel = new javax.swing.JPanel();
        sdpst301 = new javax.swing.JLabel();
        syncsrvt301Panel = new javax.swing.JPanel();
        syncsrvt301 = new javax.swing.JLabel();
        mcppt301Panel = new javax.swing.JPanel();
        mcppt301 = new javax.swing.JLabel();
        ecssims303Panel = new javax.swing.JPanel();
        ecssims303 = new javax.swing.JLabel();
        cwpt301Panel = new javax.swing.JPanel();
        cwpt301 = new javax.swing.JLabel();
        cwpt302Panel = new javax.swing.JPanel();
        cwpt302 = new javax.swing.JLabel();
        cwpt303Panel = new javax.swing.JPanel();
        cwpt303 = new javax.swing.JLabel();
        cwpt304Panel = new javax.swing.JPanel();
        cwpt304 = new javax.swing.JLabel();
        cwpt305Panel = new javax.swing.JPanel();
        cwpt305 = new javax.swing.JLabel();
        bsPanel = new javax.swing.JPanel();
        motdPanel = new javax.swing.JPanel();
        toolboxBtn = new javax.swing.JButton();
        refreshBtn1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        MainPanel.setBackground(new java.awt.Color(0, 0, 0));
        MainPanel.setMaximumSize(new java.awt.Dimension(1920, 1080));
        MainPanel.setPreferredSize(new java.awt.Dimension(1920, 1080));

        ChannelA.setBackground(new java.awt.Color(0, 0, 0));
        ChannelA.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("")), "Channel A", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        ecsa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecsa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ECS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecsa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        ecsa101Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        ecsa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecsa101.setForeground(new java.awt.Color(255, 255, 255));
        ecsa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecsa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecsa101PanelLayout = new javax.swing.GroupLayout(ecsa101Panel);
        ecsa101Panel.setLayout(ecsa101PanelLayout);
        ecsa101PanelLayout.setHorizontalGroup(
            ecsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecsa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecsa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecsa101PanelLayout.setVerticalGroup(
            ecsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecsa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        ecsa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecsa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ECS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecsa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        ecsa102Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        ecsa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecsa102.setForeground(new java.awt.Color(255, 255, 255));
        ecsa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecsa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecsa102PanelLayout = new javax.swing.GroupLayout(ecsa102Panel);
        ecsa102Panel.setLayout(ecsa102PanelLayout);
        ecsa102PanelLayout.setHorizontalGroup(
            ecsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecsa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecsa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecsa102PanelLayout.setVerticalGroup(
            ecsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecsa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        mcppa101Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppa101.setForeground(new java.awt.Color(255, 255, 255));
        mcppa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppa101PanelLayout = new javax.swing.GroupLayout(mcppa101Panel);
        mcppa101Panel.setLayout(mcppa101PanelLayout);
        mcppa101PanelLayout.setHorizontalGroup(
            mcppa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppa101PanelLayout.setVerticalGroup(
            mcppa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        mcppa102Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppa102.setForeground(new java.awt.Color(255, 255, 255));
        mcppa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppa102PanelLayout = new javax.swing.GroupLayout(mcppa102Panel);
        mcppa102Panel.setLayout(mcppa102PanelLayout);
        mcppa102PanelLayout.setHorizontalGroup(
            mcppa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppa102PanelLayout.setVerticalGroup(
            mcppa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        drpsa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        drpsa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DRPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        drpsa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        drpsa101Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        drpsa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        drpsa101.setForeground(new java.awt.Color(255, 255, 255));
        drpsa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        drpsa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout drpsa101PanelLayout = new javax.swing.GroupLayout(drpsa101Panel);
        drpsa101Panel.setLayout(drpsa101PanelLayout);
        drpsa101PanelLayout.setHorizontalGroup(
            drpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drpsa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(drpsa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        drpsa101PanelLayout.setVerticalGroup(
            drpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drpsa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppa103Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppa103Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppa103Panel.setForeground(new java.awt.Color(51, 153, 255));
        mcppa103Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppa103.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppa103.setForeground(new java.awt.Color(255, 255, 255));
        mcppa103.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppa103.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppa103PanelLayout = new javax.swing.GroupLayout(mcppa103Panel);
        mcppa103Panel.setLayout(mcppa103PanelLayout);
        mcppa103PanelLayout.setHorizontalGroup(
            mcppa103PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppa103PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppa103)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppa103PanelLayout.setVerticalGroup(
            mcppa103PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppa103, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        drpsa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        drpsa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DRPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        drpsa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        drpsa102Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        drpsa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        drpsa102.setForeground(new java.awt.Color(255, 255, 255));
        drpsa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        drpsa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout drpsa102PanelLayout = new javax.swing.GroupLayout(drpsa102Panel);
        drpsa102Panel.setLayout(drpsa102PanelLayout);
        drpsa102PanelLayout.setHorizontalGroup(
            drpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drpsa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(drpsa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        drpsa102PanelLayout.setVerticalGroup(
            drpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drpsa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppa104Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppa104Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP4", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppa104Panel.setForeground(new java.awt.Color(51, 153, 255));
        mcppa104Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppa104.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppa104.setForeground(new java.awt.Color(255, 255, 255));
        mcppa104.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppa104.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppa104PanelLayout = new javax.swing.GroupLayout(mcppa104Panel);
        mcppa104Panel.setLayout(mcppa104PanelLayout);
        mcppa104PanelLayout.setHorizontalGroup(
            mcppa104PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppa104PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppa104)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppa104PanelLayout.setVerticalGroup(
            mcppa104PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppa104, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fdpsa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        fdpsa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        fdpsa101Panel.setForeground(new java.awt.Color(102, 153, 255));
        fdpsa101Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        fdpsa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        fdpsa101.setForeground(new java.awt.Color(255, 255, 255));
        fdpsa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        fdpsa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout fdpsa101PanelLayout = new javax.swing.GroupLayout(fdpsa101Panel);
        fdpsa101Panel.setLayout(fdpsa101PanelLayout);
        fdpsa101PanelLayout.setHorizontalGroup(
            fdpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fdpsa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fdpsa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        fdpsa101PanelLayout.setVerticalGroup(
            fdpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fdpsa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa101Panel.setForeground(new java.awt.Color(102, 153, 255));
        cwpa101Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa101.setForeground(new java.awt.Color(255, 255, 255));
        cwpa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa101PanelLayout = new javax.swing.GroupLayout(cwpa101Panel);
        cwpa101Panel.setLayout(cwpa101PanelLayout);
        cwpa101PanelLayout.setHorizontalGroup(
            cwpa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa101PanelLayout.setVerticalGroup(
            cwpa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fdpsa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        fdpsa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FDPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        fdpsa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        fdpsa102Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        fdpsa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        fdpsa102.setForeground(new java.awt.Color(255, 255, 255));
        fdpsa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        fdpsa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout fdpsa102PanelLayout = new javax.swing.GroupLayout(fdpsa102Panel);
        fdpsa102Panel.setLayout(fdpsa102PanelLayout);
        fdpsa102PanelLayout.setHorizontalGroup(
            fdpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fdpsa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fdpsa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        fdpsa102PanelLayout.setVerticalGroup(
            fdpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fdpsa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa102Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa102.setForeground(new java.awt.Color(255, 255, 255));
        cwpa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa102PanelLayout = new javax.swing.GroupLayout(cwpa102Panel);
        cwpa102Panel.setLayout(cwpa102PanelLayout);
        cwpa102PanelLayout.setHorizontalGroup(
            cwpa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa102PanelLayout.setVerticalGroup(
            cwpa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sdpsa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        sdpsa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sdpsa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        sdpsa101Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sdpsa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sdpsa101.setForeground(new java.awt.Color(255, 255, 255));
        sdpsa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sdpsa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sdpsa101PanelLayout = new javax.swing.GroupLayout(sdpsa101Panel);
        sdpsa101Panel.setLayout(sdpsa101PanelLayout);
        sdpsa101PanelLayout.setHorizontalGroup(
            sdpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sdpsa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sdpsa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sdpsa101PanelLayout.setVerticalGroup(
            sdpsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sdpsa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa103Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa103Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa103Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa103Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa103.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa103.setForeground(new java.awt.Color(255, 255, 255));
        cwpa103.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa103.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa103PanelLayout = new javax.swing.GroupLayout(cwpa103Panel);
        cwpa103Panel.setLayout(cwpa103PanelLayout);
        cwpa103PanelLayout.setHorizontalGroup(
            cwpa103PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa103PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa103)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa103PanelLayout.setVerticalGroup(
            cwpa103PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa103, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sdpsa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        sdpsa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SDPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sdpsa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        sdpsa102Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sdpsa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sdpsa102.setForeground(new java.awt.Color(255, 255, 255));
        sdpsa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sdpsa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sdpsa102PanelLayout = new javax.swing.GroupLayout(sdpsa102Panel);
        sdpsa102Panel.setLayout(sdpsa102PanelLayout);
        sdpsa102PanelLayout.setHorizontalGroup(
            sdpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sdpsa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sdpsa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sdpsa102PanelLayout.setVerticalGroup(
            sdpsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sdpsa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa104Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa104Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP4", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa104Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa104Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa104.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa104.setForeground(new java.awt.Color(255, 255, 255));
        cwpa104.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa104.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa104PanelLayout = new javax.swing.GroupLayout(cwpa104Panel);
        cwpa104Panel.setLayout(cwpa104PanelLayout);
        cwpa104PanelLayout.setHorizontalGroup(
            cwpa104PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa104PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa104)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa104PanelLayout.setVerticalGroup(
            cwpa104PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa104, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sncsa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        sncsa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SYNC1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sncsa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        sncsa101Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        syncsrva101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        syncsrva101.setForeground(new java.awt.Color(255, 255, 255));
        syncsrva101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        syncsrva101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sncsa101PanelLayout = new javax.swing.GroupLayout(sncsa101Panel);
        sncsa101Panel.setLayout(sncsa101PanelLayout);
        sncsa101PanelLayout.setHorizontalGroup(
            sncsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sncsa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncsrva101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sncsa101PanelLayout.setVerticalGroup(
            sncsa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(syncsrva101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa105Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa105Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP5", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa105Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa105Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa105.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa105.setForeground(new java.awt.Color(255, 255, 255));
        cwpa105.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa105.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa105PanelLayout = new javax.swing.GroupLayout(cwpa105Panel);
        cwpa105Panel.setLayout(cwpa105PanelLayout);
        cwpa105PanelLayout.setHorizontalGroup(
            cwpa105PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa105PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa105)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa105PanelLayout.setVerticalGroup(
            cwpa105PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa105, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sncsa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        sncsa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SYNC2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sncsa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        sncsa102Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        syncsrva102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        syncsrva102.setForeground(new java.awt.Color(255, 255, 255));
        syncsrva102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        syncsrva102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sncsa102PanelLayout = new javax.swing.GroupLayout(sncsa102Panel);
        sncsa102Panel.setLayout(sncsa102PanelLayout);
        sncsa102PanelLayout.setHorizontalGroup(
            sncsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sncsa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncsrva102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sncsa102PanelLayout.setVerticalGroup(
            sncsa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(syncsrva102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa106Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa106Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP6", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa106Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa106Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa106.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa106.setForeground(new java.awt.Color(255, 255, 255));
        cwpa106.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa106.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa106PanelLayout = new javax.swing.GroupLayout(cwpa106Panel);
        cwpa106Panel.setLayout(cwpa106PanelLayout);
        cwpa106PanelLayout.setHorizontalGroup(
            cwpa106PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa106PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa106)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa106PanelLayout.setVerticalGroup(
            cwpa106PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa106, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa107Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa107Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP7", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa107Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa107Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa107.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa107.setForeground(new java.awt.Color(255, 255, 255));
        cwpa107.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa107.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa107PanelLayout = new javax.swing.GroupLayout(cwpa107Panel);
        cwpa107Panel.setLayout(cwpa107PanelLayout);
        cwpa107PanelLayout.setHorizontalGroup(
            cwpa107PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa107PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa107)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa107PanelLayout.setVerticalGroup(
            cwpa107PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa107, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa108Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa108Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP8", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa108Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa108Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa108.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa108.setForeground(new java.awt.Color(255, 255, 255));
        cwpa108.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa108.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa108PanelLayout = new javax.swing.GroupLayout(cwpa108Panel);
        cwpa108Panel.setLayout(cwpa108PanelLayout);
        cwpa108PanelLayout.setHorizontalGroup(
            cwpa108PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa108PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa108)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa108PanelLayout.setVerticalGroup(
            cwpa108PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa108, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa114Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa114Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP14", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa114Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa114Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpa114.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa114.setForeground(new java.awt.Color(255, 255, 255));
        cwpa114.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa114.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa114PanelLayout = new javax.swing.GroupLayout(cwpa114Panel);
        cwpa114Panel.setLayout(cwpa114PanelLayout);
        cwpa114PanelLayout.setHorizontalGroup(
            cwpa114PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa114PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa114)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa114PanelLayout.setVerticalGroup(
            cwpa114PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa114, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa109Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa109Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP9", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa109Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa109Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa109.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa109.setForeground(new java.awt.Color(255, 255, 255));
        cwpa109.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa109.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa109PanelLayout = new javax.swing.GroupLayout(cwpa109Panel);
        cwpa109Panel.setLayout(cwpa109PanelLayout);
        cwpa109PanelLayout.setHorizontalGroup(
            cwpa109PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa109PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa109)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa109PanelLayout.setVerticalGroup(
            cwpa109PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa109, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa115Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa115Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP15", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa115Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa115Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpa115.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa115.setForeground(new java.awt.Color(255, 255, 255));
        cwpa115.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa115.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa115PanelLayout = new javax.swing.GroupLayout(cwpa115Panel);
        cwpa115Panel.setLayout(cwpa115PanelLayout);
        cwpa115PanelLayout.setHorizontalGroup(
            cwpa115PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa115PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa115)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa115PanelLayout.setVerticalGroup(
            cwpa115PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa115, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa110Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa110Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP10", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa110Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa110Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa110.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa110.setForeground(new java.awt.Color(255, 255, 255));
        cwpa110.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa110.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa110PanelLayout = new javax.swing.GroupLayout(cwpa110Panel);
        cwpa110Panel.setLayout(cwpa110PanelLayout);
        cwpa110PanelLayout.setHorizontalGroup(
            cwpa110PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa110PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa110)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa110PanelLayout.setVerticalGroup(
            cwpa110PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa110, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sppa101Panel.setBackground(new java.awt.Color(0, 0, 0));
        sppa101Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SPP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sppa101Panel.setForeground(new java.awt.Color(51, 153, 255));
        sppa101Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sppa101.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sppa101.setForeground(new java.awt.Color(255, 255, 255));
        sppa101.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sppa101.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sppa101PanelLayout = new javax.swing.GroupLayout(sppa101Panel);
        sppa101Panel.setLayout(sppa101PanelLayout);
        sppa101PanelLayout.setHorizontalGroup(
            sppa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sppa101PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sppa101)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sppa101PanelLayout.setVerticalGroup(
            sppa101PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sppa101, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa111Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa111Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP11", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa111Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa111Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa111.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa111.setForeground(new java.awt.Color(255, 255, 255));
        cwpa111.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa111.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa111PanelLayout = new javax.swing.GroupLayout(cwpa111Panel);
        cwpa111Panel.setLayout(cwpa111PanelLayout);
        cwpa111PanelLayout.setHorizontalGroup(
            cwpa111PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa111PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa111)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa111PanelLayout.setVerticalGroup(
            cwpa111PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa111, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sppa102Panel.setBackground(new java.awt.Color(0, 0, 0));
        sppa102Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SPP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sppa102Panel.setForeground(new java.awt.Color(51, 153, 255));
        sppa102Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sppa102.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sppa102.setForeground(new java.awt.Color(255, 255, 255));
        sppa102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sppa102.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sppa102PanelLayout = new javax.swing.GroupLayout(sppa102Panel);
        sppa102Panel.setLayout(sppa102PanelLayout);
        sppa102PanelLayout.setHorizontalGroup(
            sppa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sppa102PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sppa102)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sppa102PanelLayout.setVerticalGroup(
            sppa102PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sppa102, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa112Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa112Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP12", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa112Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa112Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa112.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa112.setForeground(new java.awt.Color(255, 255, 255));
        cwpa112.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa112.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa112PanelLayout = new javax.swing.GroupLayout(cwpa112Panel);
        cwpa112Panel.setLayout(cwpa112PanelLayout);
        cwpa112PanelLayout.setHorizontalGroup(
            cwpa112PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa112PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa112)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa112PanelLayout.setVerticalGroup(
            cwpa112PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa112, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpa113Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpa113Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP13", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpa113Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpa113Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpa113.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpa113.setForeground(new java.awt.Color(255, 255, 255));
        cwpa113.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpa113.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpa113PanelLayout = new javax.swing.GroupLayout(cwpa113Panel);
        cwpa113Panel.setLayout(cwpa113PanelLayout);
        cwpa113PanelLayout.setHorizontalGroup(
            cwpa113PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpa113PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpa113)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpa113PanelLayout.setVerticalGroup(
            cwpa113PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpa113, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout ChannelALayout = new javax.swing.GroupLayout(ChannelA);
        ChannelA.setLayout(ChannelALayout);
        ChannelALayout.setHorizontalGroup(
            ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ChannelALayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelALayout.createSequentialGroup()
                        .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cwpa101Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ecsa101Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cwpa102Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ecsa102Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(ChannelALayout.createSequentialGroup()
                        .addComponent(sncsa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sncsa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ChannelALayout.createSequentialGroup()
                        .addComponent(cwpa109Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpa110Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelALayout.createSequentialGroup()
                        .addComponent(cwpa111Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpa112Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpa113Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpa114Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpa115Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(ChannelALayout.createSequentialGroup()
                        .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ChannelALayout.createSequentialGroup()
                                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpa103Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(drpsa101Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpa104Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(drpsa102Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cwpa105Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fdpsa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpa106Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fdpsa102Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpa107Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(sdpsa101Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ChannelALayout.createSequentialGroup()
                                .addComponent(mcppa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppa103Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppa104Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(sppa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cwpa108Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sdpsa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sppa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        ChannelALayout.setVerticalGroup(
            ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ChannelALayout.createSequentialGroup()
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ecsa102Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ecsa101Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drpsa101Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drpsa102Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fdpsa101Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fdpsa102Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sdpsa101Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sdpsa102Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(mcppa104Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppa103Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppa102Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppa101Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sppa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sppa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(sncsa101Panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sncsa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cwpa108Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa107Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa106Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa105Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa104Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa103Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa102Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa101Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ChannelALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cwpa112Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa111Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa113Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa114Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa115Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa110Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpa109Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        ChannelB.setBackground(new java.awt.Color(0, 0, 0));
        ChannelB.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("")), "Channel B", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        ecsb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecsb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ECS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecsb601Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        ecsb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecsb601.setForeground(new java.awt.Color(255, 255, 255));
        ecsb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecsb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecsb601PanelLayout = new javax.swing.GroupLayout(ecsb601Panel);
        ecsb601Panel.setLayout(ecsb601PanelLayout);
        ecsb601PanelLayout.setHorizontalGroup(
            ecsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecsb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecsb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecsb601PanelLayout.setVerticalGroup(
            ecsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecsb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        ecsb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecsb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ECS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecsb602Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        ecsb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecsb602.setForeground(new java.awt.Color(255, 255, 255));
        ecsb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecsb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecsb602PanelLayout = new javax.swing.GroupLayout(ecsb602Panel);
        ecsb602Panel.setLayout(ecsb602PanelLayout);
        ecsb602PanelLayout.setHorizontalGroup(
            ecsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecsb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecsb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecsb602PanelLayout.setVerticalGroup(
            ecsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecsb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppb601Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppb601.setForeground(new java.awt.Color(255, 255, 255));
        mcppb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppb601PanelLayout = new javax.swing.GroupLayout(mcppb601Panel);
        mcppb601Panel.setLayout(mcppb601PanelLayout);
        mcppb601PanelLayout.setHorizontalGroup(
            mcppb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppb601PanelLayout.setVerticalGroup(
            mcppb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppb602Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppb602.setForeground(new java.awt.Color(255, 255, 255));
        mcppb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppb602PanelLayout = new javax.swing.GroupLayout(mcppb602Panel);
        mcppb602Panel.setLayout(mcppb602PanelLayout);
        mcppb602PanelLayout.setHorizontalGroup(
            mcppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppb602PanelLayout.setVerticalGroup(
            mcppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        drpsb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        drpsb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DRPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        drpsb601Panel.setPreferredSize(new java.awt.Dimension(103, 116));

        drpsb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        drpsb601.setForeground(new java.awt.Color(255, 255, 255));
        drpsb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        drpsb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout drpsb601PanelLayout = new javax.swing.GroupLayout(drpsb601Panel);
        drpsb601Panel.setLayout(drpsb601PanelLayout);
        drpsb601PanelLayout.setHorizontalGroup(
            drpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drpsb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(drpsb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        drpsb601PanelLayout.setVerticalGroup(
            drpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drpsb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppb603Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppb603Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppb603Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppb603.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppb603.setForeground(new java.awt.Color(255, 255, 255));
        mcppb603.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppb603.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppb603PanelLayout = new javax.swing.GroupLayout(mcppb603Panel);
        mcppb603Panel.setLayout(mcppb603PanelLayout);
        mcppb603PanelLayout.setHorizontalGroup(
            mcppb603PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppb603PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppb603)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppb603PanelLayout.setVerticalGroup(
            mcppb603PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppb603, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        drpsb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        drpsb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DRPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        drpsb602Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        drpsb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        drpsb602.setForeground(new java.awt.Color(255, 255, 255));
        drpsb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        drpsb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout drpsb602PanelLayout = new javax.swing.GroupLayout(drpsb602Panel);
        drpsb602Panel.setLayout(drpsb602PanelLayout);
        drpsb602PanelLayout.setHorizontalGroup(
            drpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drpsb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(drpsb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        drpsb602PanelLayout.setVerticalGroup(
            drpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drpsb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppb604Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppb604Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP4", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppb604Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        mcppb604.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppb604.setForeground(new java.awt.Color(255, 255, 255));
        mcppb604.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppb604.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppb604PanelLayout = new javax.swing.GroupLayout(mcppb604Panel);
        mcppb604Panel.setLayout(mcppb604PanelLayout);
        mcppb604PanelLayout.setHorizontalGroup(
            mcppb604PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppb604PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppb604)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppb604PanelLayout.setVerticalGroup(
            mcppb604PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppb604, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fdpsb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        fdpsb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        fdpsb601Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        fdpsb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        fdpsb601.setForeground(new java.awt.Color(255, 255, 255));
        fdpsb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        fdpsb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout fdpsb601PanelLayout = new javax.swing.GroupLayout(fdpsb601Panel);
        fdpsb601Panel.setLayout(fdpsb601PanelLayout);
        fdpsb601PanelLayout.setHorizontalGroup(
            fdpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fdpsb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fdpsb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        fdpsb601PanelLayout.setVerticalGroup(
            fdpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fdpsb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb601Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb601.setForeground(new java.awt.Color(255, 255, 255));
        cwpb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb601PanelLayout = new javax.swing.GroupLayout(cwpb601Panel);
        cwpb601Panel.setLayout(cwpb601PanelLayout);
        cwpb601PanelLayout.setHorizontalGroup(
            cwpb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb601PanelLayout.setVerticalGroup(
            cwpb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fdpsb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        fdpsb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FDPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        fdpsb602Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        fdpsb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        fdpsb602.setForeground(new java.awt.Color(255, 255, 255));
        fdpsb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        fdpsb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout fdpsb602PanelLayout = new javax.swing.GroupLayout(fdpsb602Panel);
        fdpsb602Panel.setLayout(fdpsb602PanelLayout);
        fdpsb602PanelLayout.setHorizontalGroup(
            fdpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fdpsb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fdpsb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        fdpsb602PanelLayout.setVerticalGroup(
            fdpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fdpsb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb602Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb602.setForeground(new java.awt.Color(255, 255, 255));
        cwpb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb602PanelLayout = new javax.swing.GroupLayout(cwpb602Panel);
        cwpb602Panel.setLayout(cwpb602PanelLayout);
        cwpb602PanelLayout.setHorizontalGroup(
            cwpb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb602PanelLayout.setVerticalGroup(
            cwpb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sdpsb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        sdpsb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sdpsb601Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sdpsb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sdpsb601.setForeground(new java.awt.Color(255, 255, 255));
        sdpsb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sdpsb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sdpsb601PanelLayout = new javax.swing.GroupLayout(sdpsb601Panel);
        sdpsb601Panel.setLayout(sdpsb601PanelLayout);
        sdpsb601PanelLayout.setHorizontalGroup(
            sdpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sdpsb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sdpsb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sdpsb601PanelLayout.setVerticalGroup(
            sdpsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sdpsb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb603Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb603Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb603Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb603.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb603.setForeground(new java.awt.Color(255, 255, 255));
        cwpb603.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb603.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb603PanelLayout = new javax.swing.GroupLayout(cwpb603Panel);
        cwpb603Panel.setLayout(cwpb603PanelLayout);
        cwpb603PanelLayout.setHorizontalGroup(
            cwpb603PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb603PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb603)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb603PanelLayout.setVerticalGroup(
            cwpb603PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb603, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sdpsb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        sdpsb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SDPS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sdpsb602Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sdpsb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sdpsb602.setForeground(new java.awt.Color(255, 255, 255));
        sdpsb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sdpsb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sdpsb602PanelLayout = new javax.swing.GroupLayout(sdpsb602Panel);
        sdpsb602Panel.setLayout(sdpsb602PanelLayout);
        sdpsb602PanelLayout.setHorizontalGroup(
            sdpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sdpsb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sdpsb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sdpsb602PanelLayout.setVerticalGroup(
            sdpsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sdpsb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb604Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb604Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP4", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb604Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb604.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb604.setForeground(new java.awt.Color(255, 255, 255));
        cwpb604.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb604.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb604PanelLayout = new javax.swing.GroupLayout(cwpb604Panel);
        cwpb604Panel.setLayout(cwpb604PanelLayout);
        cwpb604PanelLayout.setHorizontalGroup(
            cwpb604PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb604PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb604)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb604PanelLayout.setVerticalGroup(
            cwpb604PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb604, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sncsb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        sncsb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SYNC1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sncsb601Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        syncsrvb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        syncsrvb601.setForeground(new java.awt.Color(255, 255, 255));
        syncsrvb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        syncsrvb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sncsb601PanelLayout = new javax.swing.GroupLayout(sncsb601Panel);
        sncsb601Panel.setLayout(sncsb601PanelLayout);
        sncsb601PanelLayout.setHorizontalGroup(
            sncsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sncsb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncsrvb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sncsb601PanelLayout.setVerticalGroup(
            sncsb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(syncsrvb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb605Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb605Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP5", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb605Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb605.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb605.setForeground(new java.awt.Color(255, 255, 255));
        cwpb605.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb605.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb605PanelLayout = new javax.swing.GroupLayout(cwpb605Panel);
        cwpb605Panel.setLayout(cwpb605PanelLayout);
        cwpb605PanelLayout.setHorizontalGroup(
            cwpb605PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb605PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb605)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb605PanelLayout.setVerticalGroup(
            cwpb605PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb605, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sncsb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        sncsb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SYNC2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sncsb602Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        syncsrvb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        syncsrvb602.setForeground(new java.awt.Color(255, 255, 255));
        syncsrvb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        syncsrvb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sncsb602PanelLayout = new javax.swing.GroupLayout(sncsb602Panel);
        sncsb602Panel.setLayout(sncsb602PanelLayout);
        sncsb602PanelLayout.setHorizontalGroup(
            sncsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sncsb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncsrvb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sncsb602PanelLayout.setVerticalGroup(
            sncsb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(syncsrvb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb606Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb606Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP6", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb606Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb606.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb606.setForeground(new java.awt.Color(255, 255, 255));
        cwpb606.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb606.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb606PanelLayout = new javax.swing.GroupLayout(cwpb606Panel);
        cwpb606Panel.setLayout(cwpb606PanelLayout);
        cwpb606PanelLayout.setHorizontalGroup(
            cwpb606PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb606PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb606)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb606PanelLayout.setVerticalGroup(
            cwpb606PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb606, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb607Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb607Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP7", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb607Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb607.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb607.setForeground(new java.awt.Color(255, 255, 255));
        cwpb607.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb607.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb607PanelLayout = new javax.swing.GroupLayout(cwpb607Panel);
        cwpb607Panel.setLayout(cwpb607PanelLayout);
        cwpb607PanelLayout.setHorizontalGroup(
            cwpb607PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb607PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb607)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb607PanelLayout.setVerticalGroup(
            cwpb607PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb607, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb608Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb608Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP8", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb608Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb608.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb608.setForeground(new java.awt.Color(255, 255, 255));
        cwpb608.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb608.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb608PanelLayout = new javax.swing.GroupLayout(cwpb608Panel);
        cwpb608Panel.setLayout(cwpb608PanelLayout);
        cwpb608PanelLayout.setHorizontalGroup(
            cwpb608PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb608PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb608)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb608PanelLayout.setVerticalGroup(
            cwpb608PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb608, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb614Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb614Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP14", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb614Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpb614.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb614.setForeground(new java.awt.Color(255, 255, 255));
        cwpb614.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb614.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb614PanelLayout = new javax.swing.GroupLayout(cwpb614Panel);
        cwpb614Panel.setLayout(cwpb614PanelLayout);
        cwpb614PanelLayout.setHorizontalGroup(
            cwpb614PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb614PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb614)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb614PanelLayout.setVerticalGroup(
            cwpb614PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb614, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb609Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb609Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP9", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb609Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb609.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb609.setForeground(new java.awt.Color(255, 255, 255));
        cwpb609.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb609.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb609PanelLayout = new javax.swing.GroupLayout(cwpb609Panel);
        cwpb609Panel.setLayout(cwpb609PanelLayout);
        cwpb609PanelLayout.setHorizontalGroup(
            cwpb609PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb609PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb609)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb609PanelLayout.setVerticalGroup(
            cwpb609PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb609, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        pppb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        pppb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PPP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        pppb602Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        pppb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        pppb602.setForeground(new java.awt.Color(255, 255, 255));
        pppb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        pppb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout pppb602PanelLayout = new javax.swing.GroupLayout(pppb602Panel);
        pppb602Panel.setLayout(pppb602PanelLayout);
        pppb602PanelLayout.setHorizontalGroup(
            pppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pppb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pppb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        pppb602PanelLayout.setVerticalGroup(
            pppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pppb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb610Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb610Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP10", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb610Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb610.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb610.setForeground(new java.awt.Color(255, 255, 255));
        cwpb610.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb610.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb610PanelLayout = new javax.swing.GroupLayout(cwpb610Panel);
        cwpb610Panel.setLayout(cwpb610PanelLayout);
        cwpb610PanelLayout.setHorizontalGroup(
            cwpb610PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb610PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb610)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb610PanelLayout.setVerticalGroup(
            cwpb610PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb610, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sppb601Panel.setBackground(new java.awt.Color(0, 0, 0));
        sppb601Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SPP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sppb601Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sppb601.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sppb601.setForeground(new java.awt.Color(255, 255, 255));
        sppb601.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sppb601.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sppb601PanelLayout = new javax.swing.GroupLayout(sppb601Panel);
        sppb601Panel.setLayout(sppb601PanelLayout);
        sppb601PanelLayout.setHorizontalGroup(
            sppb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sppb601PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sppb601)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sppb601PanelLayout.setVerticalGroup(
            sppb601PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sppb601, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sppb602Panel.setBackground(new java.awt.Color(0, 0, 0));
        sppb602Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SPP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sppb602Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sppb602.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sppb602.setForeground(new java.awt.Color(255, 255, 255));
        sppb602.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sppb602.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sppb602PanelLayout = new javax.swing.GroupLayout(sppb602Panel);
        sppb602Panel.setLayout(sppb602PanelLayout);
        sppb602PanelLayout.setHorizontalGroup(
            sppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sppb602PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sppb602)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sppb602PanelLayout.setVerticalGroup(
            sppb602PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sppb602, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb611Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb611Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP11", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb611Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb611.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb611.setForeground(new java.awt.Color(255, 255, 255));
        cwpb611.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb611.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb611PanelLayout = new javax.swing.GroupLayout(cwpb611Panel);
        cwpb611Panel.setLayout(cwpb611PanelLayout);
        cwpb611PanelLayout.setHorizontalGroup(
            cwpb611PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb611PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb611)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb611PanelLayout.setVerticalGroup(
            cwpb611PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb611, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb612Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb612Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP12", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb612Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb612.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb612.setForeground(new java.awt.Color(255, 255, 255));
        cwpb612.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb612.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb612PanelLayout = new javax.swing.GroupLayout(cwpb612Panel);
        cwpb612Panel.setLayout(cwpb612PanelLayout);
        cwpb612PanelLayout.setHorizontalGroup(
            cwpb612PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb612PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb612)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb612PanelLayout.setVerticalGroup(
            cwpb612PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb612, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpb613Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpb613Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP13", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpb613Panel.setPreferredSize(new java.awt.Dimension(103, 120));

        cwpb613.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpb613.setForeground(new java.awt.Color(255, 255, 255));
        cwpb613.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpb613.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpb613PanelLayout = new javax.swing.GroupLayout(cwpb613Panel);
        cwpb613Panel.setLayout(cwpb613PanelLayout);
        cwpb613PanelLayout.setHorizontalGroup(
            cwpb613PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpb613PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpb613)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpb613PanelLayout.setVerticalGroup(
            cwpb613PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpb613, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout ChannelBLayout = new javax.swing.GroupLayout(ChannelB);
        ChannelB.setLayout(ChannelBLayout);
        ChannelBLayout.setHorizontalGroup(
            ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ChannelBLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelBLayout.createSequentialGroup()
                        .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cwpb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ecsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cwpb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ecsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(ChannelBLayout.createSequentialGroup()
                        .addComponent(cwpb609Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpb610Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(ChannelBLayout.createSequentialGroup()
                        .addComponent(sncsb601Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sncsb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelBLayout.createSequentialGroup()
                        .addComponent(cwpb611Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpb612Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpb613Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpb614Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(ChannelBLayout.createSequentialGroup()
                                .addGap(151, 151, 151)
                                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpb608Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(sdpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ChannelBLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pppb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(ChannelBLayout.createSequentialGroup()
                        .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(ChannelBLayout.createSequentialGroup()
                                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpb603Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(drpsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpb604Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(drpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cwpb605Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fdpsb601Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cwpb606Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(fdpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(ChannelBLayout.createSequentialGroup()
                                .addComponent(mcppb601Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppb603Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mcppb604Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cwpb607Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sdpsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, ChannelBLayout.createSequentialGroup()
                                .addComponent(sppb601Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(sppb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        ChannelBLayout.setVerticalGroup(
            ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ChannelBLayout.createSequentialGroup()
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(sdpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sdpsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fdpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fdpsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drpsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drpsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ecsb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ecsb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(sppb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppb604Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppb603Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(mcppb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sppb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(sncsb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sncsb601Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cwpb608Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb607Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb606Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb605Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb604Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb603Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb602Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb601Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cwpb612Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb611Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb613Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpb614Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pppb602Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(ChannelBLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cwpb609Panel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cwpb610Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Legend.setBackground(new java.awt.Color(0, 0, 0));
        Legend.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 24), new java.awt.Color(0, 153, 255))); // NOI18N

        jLabel1.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 153, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/greenup45px.png"))); // NOI18N
        jLabel1.setText("FREE");

        jLabel2.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(102, 153, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangeup45px.png"))); // NOI18N
        jLabel2.setText("LOADED UP");

        jLabel3.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(102, 153, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/orangedown45px.png"))); // NOI18N
        jLabel3.setText("FREE DOWN");

        jLabel4.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 153, 255));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/reddown45px.png"))); // NOI18N
        jLabel4.setText("DOWN");

        jLabel8.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 153, 255));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/dist45px.png"))); // NOI18N
        jLabel8.setText("DISTRIBUTING");
        jLabel8.setToolTipText("");

        jLabel5.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 153, 255));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        jLabel5.setText("UNKNOWN");

        javax.swing.GroupLayout LegendLayout = new javax.swing.GroupLayout(Legend);
        Legend.setLayout(LegendLayout);
        LegendLayout.setHorizontalGroup(
            LegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LegendLayout.createSequentialGroup()
                .addGroup(LegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel8)
                    .addComponent(jLabel5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        LegendLayout.setVerticalGroup(
            LegendLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LegendLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SupportS.setBackground(new java.awt.Color(0, 0, 0));
        SupportS.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Channel T Support", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        sims301Panel.setBackground(new java.awt.Color(0, 0, 0));
        sims301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SIMS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sims301Panel.setForeground(new java.awt.Color(51, 153, 255));
        sims301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        ecssims301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecssims301.setForeground(new java.awt.Color(255, 255, 255));
        ecssims301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecssims301.setText("-- ");
        ecssims301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sims301PanelLayout = new javax.swing.GroupLayout(sims301Panel);
        sims301Panel.setLayout(sims301PanelLayout);
        sims301PanelLayout.setHorizontalGroup(
            sims301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sims301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecssims301)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        sims301PanelLayout.setVerticalGroup(
            sims301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecssims301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sims302Panel.setBackground(new java.awt.Color(0, 0, 0));
        sims302Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SIMS2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sims302Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        ecssims302.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecssims302.setForeground(new java.awt.Color(255, 255, 255));
        ecssims302.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecssims302.setText("-- ");
        ecssims302.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sims302PanelLayout = new javax.swing.GroupLayout(sims302Panel);
        sims302Panel.setLayout(sims302PanelLayout);
        sims302PanelLayout.setHorizontalGroup(
            sims302PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sims302PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecssims302)
                .addContainerGap(21, Short.MAX_VALUE))
        );
        sims302PanelLayout.setVerticalGroup(
            sims302PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecssims302, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        supssPanel.setBackground(new java.awt.Color(0, 0, 0));
        supssPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SUPSS", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        supssPanel.setForeground(new java.awt.Color(51, 153, 255));
        supssPanel.setPreferredSize(new java.awt.Dimension(115, 120));

        supss301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        supss301.setForeground(new java.awt.Color(255, 255, 255));
        supss301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        supss301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout supssPanelLayout = new javax.swing.GroupLayout(supssPanel);
        supssPanel.setLayout(supssPanelLayout);
        supssPanelLayout.setHorizontalGroup(
            supssPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(supssPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(supss301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        supssPanelLayout.setVerticalGroup(
            supssPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(supss301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        oiiflc1Panel.setBackground(new java.awt.Color(0, 0, 0));
        oiiflc1Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LC", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        oiiflc1Panel.setForeground(new java.awt.Color(51, 153, 255));
        oiiflc1Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        oiiflc1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        oiiflc1.setForeground(new java.awt.Color(255, 255, 255));
        oiiflc1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        oiiflc1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout oiiflc1PanelLayout = new javax.swing.GroupLayout(oiiflc1Panel);
        oiiflc1Panel.setLayout(oiiflc1PanelLayout);
        oiiflc1PanelLayout.setHorizontalGroup(
            oiiflc1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(oiiflc1PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(oiiflc1)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        oiiflc1PanelLayout.setVerticalGroup(
            oiiflc1PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(oiiflc1, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        ecst301Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecst301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ECS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecst301Panel.setForeground(new java.awt.Color(51, 153, 255));
        ecst301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        ecst301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecst301.setForeground(new java.awt.Color(255, 255, 255));
        ecst301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecst301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecst301PanelLayout = new javax.swing.GroupLayout(ecst301Panel);
        ecst301Panel.setLayout(ecst301PanelLayout);
        ecst301PanelLayout.setHorizontalGroup(
            ecst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecst301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecst301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecst301PanelLayout.setVerticalGroup(
            ecst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecst301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        drpst301Panel.setBackground(new java.awt.Color(0, 0, 0));
        drpst301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DRPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        drpst301Panel.setForeground(new java.awt.Color(51, 153, 255));
        drpst301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        drpst301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        drpst301.setForeground(new java.awt.Color(255, 255, 255));
        drpst301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        drpst301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout drpst301PanelLayout = new javax.swing.GroupLayout(drpst301Panel);
        drpst301Panel.setLayout(drpst301PanelLayout);
        drpst301PanelLayout.setHorizontalGroup(
            drpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(drpst301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(drpst301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        drpst301PanelLayout.setVerticalGroup(
            drpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(drpst301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        fdpst301Panel.setBackground(new java.awt.Color(0, 0, 0));
        fdpst301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        fdpst301Panel.setForeground(new java.awt.Color(51, 153, 255));
        fdpst301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        fdpst301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        fdpst301.setForeground(new java.awt.Color(255, 255, 255));
        fdpst301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        fdpst301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout fdpst301PanelLayout = new javax.swing.GroupLayout(fdpst301Panel);
        fdpst301Panel.setLayout(fdpst301PanelLayout);
        fdpst301PanelLayout.setHorizontalGroup(
            fdpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fdpst301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fdpst301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        fdpst301PanelLayout.setVerticalGroup(
            fdpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(fdpst301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        sdpst301Panel.setBackground(new java.awt.Color(0, 0, 0));
        sdpst301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SDPS1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        sdpst301Panel.setForeground(new java.awt.Color(51, 153, 255));
        sdpst301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        sdpst301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        sdpst301.setForeground(new java.awt.Color(255, 255, 255));
        sdpst301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        sdpst301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout sdpst301PanelLayout = new javax.swing.GroupLayout(sdpst301Panel);
        sdpst301Panel.setLayout(sdpst301PanelLayout);
        sdpst301PanelLayout.setHorizontalGroup(
            sdpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sdpst301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sdpst301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        sdpst301PanelLayout.setVerticalGroup(
            sdpst301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sdpst301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        syncsrvt301Panel.setBackground(new java.awt.Color(0, 0, 0));
        syncsrvt301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SYNC1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        syncsrvt301Panel.setForeground(new java.awt.Color(51, 153, 255));
        syncsrvt301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        syncsrvt301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        syncsrvt301.setForeground(new java.awt.Color(255, 255, 255));
        syncsrvt301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        syncsrvt301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout syncsrvt301PanelLayout = new javax.swing.GroupLayout(syncsrvt301Panel);
        syncsrvt301Panel.setLayout(syncsrvt301PanelLayout);
        syncsrvt301PanelLayout.setHorizontalGroup(
            syncsrvt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(syncsrvt301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(syncsrvt301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        syncsrvt301PanelLayout.setVerticalGroup(
            syncsrvt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(syncsrvt301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        mcppt301Panel.setBackground(new java.awt.Color(0, 0, 0));
        mcppt301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MCPP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        mcppt301Panel.setForeground(new java.awt.Color(51, 153, 255));
        mcppt301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        mcppt301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        mcppt301.setForeground(new java.awt.Color(255, 255, 255));
        mcppt301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        mcppt301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mcppt301PanelLayout = new javax.swing.GroupLayout(mcppt301Panel);
        mcppt301Panel.setLayout(mcppt301PanelLayout);
        mcppt301PanelLayout.setHorizontalGroup(
            mcppt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcppt301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mcppt301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        mcppt301PanelLayout.setVerticalGroup(
            mcppt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mcppt301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        ecssims303Panel.setBackground(new java.awt.Color(0, 0, 0));
        ecssims303Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SIMS3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        ecssims303Panel.setForeground(new java.awt.Color(51, 153, 255));
        ecssims303Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        ecssims303.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        ecssims303.setForeground(new java.awt.Color(255, 255, 255));
        ecssims303.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        ecssims303.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout ecssims303PanelLayout = new javax.swing.GroupLayout(ecssims303Panel);
        ecssims303Panel.setLayout(ecssims303PanelLayout);
        ecssims303PanelLayout.setHorizontalGroup(
            ecssims303PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ecssims303PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ecssims303)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        ecssims303PanelLayout.setVerticalGroup(
            ecssims303PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ecssims303, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpt301Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpt301Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP1", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpt301Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpt301Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpt301.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpt301.setForeground(new java.awt.Color(255, 255, 255));
        cwpt301.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpt301.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpt301PanelLayout = new javax.swing.GroupLayout(cwpt301Panel);
        cwpt301Panel.setLayout(cwpt301PanelLayout);
        cwpt301PanelLayout.setHorizontalGroup(
            cwpt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpt301PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpt301)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpt301PanelLayout.setVerticalGroup(
            cwpt301PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpt301, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpt302Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpt302Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP2", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpt302Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpt302Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpt302.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpt302.setForeground(new java.awt.Color(255, 255, 255));
        cwpt302.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpt302.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpt302PanelLayout = new javax.swing.GroupLayout(cwpt302Panel);
        cwpt302Panel.setLayout(cwpt302PanelLayout);
        cwpt302PanelLayout.setHorizontalGroup(
            cwpt302PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpt302PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpt302)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpt302PanelLayout.setVerticalGroup(
            cwpt302PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpt302, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpt303Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpt303Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP3", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpt303Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpt303Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpt303.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpt303.setForeground(new java.awt.Color(255, 255, 255));
        cwpt303.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpt303.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpt303PanelLayout = new javax.swing.GroupLayout(cwpt303Panel);
        cwpt303Panel.setLayout(cwpt303PanelLayout);
        cwpt303PanelLayout.setHorizontalGroup(
            cwpt303PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpt303PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpt303)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpt303PanelLayout.setVerticalGroup(
            cwpt303PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpt303, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpt304Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpt304Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP4", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpt304Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpt304Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpt304.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpt304.setForeground(new java.awt.Color(255, 255, 255));
        cwpt304.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpt304.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpt304PanelLayout = new javax.swing.GroupLayout(cwpt304Panel);
        cwpt304Panel.setLayout(cwpt304PanelLayout);
        cwpt304PanelLayout.setHorizontalGroup(
            cwpt304PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpt304PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpt304)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpt304PanelLayout.setVerticalGroup(
            cwpt304PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpt304, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        cwpt305Panel.setBackground(new java.awt.Color(0, 0, 0));
        cwpt305Panel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CWP5", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(102, 153, 255))); // NOI18N
        cwpt305Panel.setForeground(new java.awt.Color(51, 153, 255));
        cwpt305Panel.setPreferredSize(new java.awt.Dimension(115, 120));

        cwpt305.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        cwpt305.setForeground(new java.awt.Color(255, 255, 255));
        cwpt305.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/unknown45px.png"))); // NOI18N
        cwpt305.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout cwpt305PanelLayout = new javax.swing.GroupLayout(cwpt305Panel);
        cwpt305Panel.setLayout(cwpt305PanelLayout);
        cwpt305PanelLayout.setHorizontalGroup(
            cwpt305PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cwpt305PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cwpt305)
                .addContainerGap(48, Short.MAX_VALUE))
        );
        cwpt305PanelLayout.setVerticalGroup(
            cwpt305PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(cwpt305, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        javax.swing.GroupLayout SupportSLayout = new javax.swing.GroupLayout(SupportS);
        SupportS.setLayout(SupportSLayout);
        SupportSLayout.setHorizontalGroup(
            SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SupportSLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(SupportSLayout.createSequentialGroup()
                        .addComponent(sims301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(sims302Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ecssims303Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addComponent(supssPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SupportSLayout.createSequentialGroup()
                        .addComponent(oiiflc1Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(ecst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(drpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fdpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(SupportSLayout.createSequentialGroup()
                        .addComponent(sdpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(syncsrvt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mcppt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cwpt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(SupportSLayout.createSequentialGroup()
                        .addComponent(cwpt302Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpt303Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cwpt304Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cwpt305Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        SupportSLayout.setVerticalGroup(
            SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SupportSLayout.createSequentialGroup()
                .addGroup(SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sims301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sims302Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(supssPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ecssims303Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(oiiflc1Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ecst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fdpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sdpst301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(syncsrvt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mcppt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpt301Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(SupportSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cwpt303Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpt302Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpt305Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cwpt304Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        bsPanel.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout bsPanelLayout = new javax.swing.GroupLayout(bsPanel);
        bsPanel.setLayout(bsPanelLayout);
        bsPanelLayout.setHorizontalGroup(
            bsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        bsPanelLayout.setVerticalGroup(
            bsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        motdPanel = new motd();
        motdPanel.setBackground(new java.awt.Color(0, 0, 0));

        javax.swing.GroupLayout motdPanelLayout = new javax.swing.GroupLayout(motdPanel);
        motdPanel.setLayout(motdPanelLayout);
        motdPanelLayout.setHorizontalGroup(
            motdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        motdPanelLayout.setVerticalGroup(
            motdPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 74, Short.MAX_VALUE)
        );

        toolboxBtn.setBackground(new java.awt.Color(51, 51, 51));
        toolboxBtn.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        toolboxBtn.setForeground(new java.awt.Color(255, 255, 255));
        toolboxBtn.setText("TOOLBOX");
        toolboxBtn.setFocusable(false);
        toolboxBtn.setPreferredSize(new java.awt.Dimension(276, 50));
        toolboxBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toolboxBtnActionPerformed(evt);
            }
        });

        refreshBtn1.setBackground(new java.awt.Color(51, 51, 51));
        refreshBtn1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        refreshBtn1.setForeground(new java.awt.Color(255, 255, 255));
        refreshBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/refresh.png"))); // NOI18N
        refreshBtn1.setText("REFRESH");
        refreshBtn1.setFocusable(false);
        refreshBtn1.setPreferredSize(new java.awt.Dimension(276, 50));
        refreshBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshBtn1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(motdPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(ChannelB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(ChannelA, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(MainPanelLayout.createSequentialGroup()
                                .addComponent(toolboxBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(refreshBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(SupportS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Legend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(767, 767, 767)
                .addComponent(bsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ChannelA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SupportS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ChannelB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(MainPanelLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(Legend, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(toolboxBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(refreshBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(motdPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        SupportS.getAccessibleContext().setAccessibleName("SupportS");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(MainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refreshBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshBtn1ActionPerformed
        // TODO add your handling code here:
        workers = new ArrayList<PCSWorker>();
        setPCS();
        startPolling();
        motdPanel = new motd();
        motd.setMotd();
    }//GEN-LAST:event_refreshBtn1ActionPerformed

    private void toolboxBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toolboxBtnActionPerformed
        ToolboxGUI tg = new ToolboxGUI();
        tg.setLocationRelativeTo(this);
        tg.setVisible(true);
    }//GEN-LAST:event_toolboxBtnActionPerformed
    private void ecsa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecsa101", pcsMap.get("ecsa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void ecsa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecsa102", pcsMap.get("ecsa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void drpsa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("drpsa101", pcsMap.get("drpsa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void drpsa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("drpsa102", pcsMap.get("drpsa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void fdpsa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("fdpsa101", pcsMap.get("fdpsa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void fdpsa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("fdpsa102", pcsMap.get("fdpsa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sdpsa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sdpsa101", pcsMap.get("sdpsa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sdpsa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sdpsa102", pcsMap.get("sdpsa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void syncsrva101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("syncsrva101", pcsMap.get("syncsrva101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void syncsrva102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("syncsrva102", pcsMap.get("syncsrva102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppa101", pcsMap.get("mcppa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppa102", pcsMap.get("mcppa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppa103MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppa103", pcsMap.get("mcppa103"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppa104MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppa104", pcsMap.get("mcppa104"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sppa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sppa101", pcsMap.get("sppa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sppa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sppa102", pcsMap.get("sppa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa101MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa101", pcsMap.get("cwpa101"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa102MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa102", pcsMap.get("cwpa102"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa103MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa103", pcsMap.get("cwpa103"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa104MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa104", pcsMap.get("cwpa104"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa105MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa105", pcsMap.get("cwpa105"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa106MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa106", pcsMap.get("cwpa106"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa107MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa107", pcsMap.get("cwpa107"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa108MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa108", pcsMap.get("cwpa108"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa109MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa109", pcsMap.get("cwpa109"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa110MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa110", pcsMap.get("cwpa110"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa111MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa111", pcsMap.get("cwpa111"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa112MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa112", pcsMap.get("cwpa112"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa113MouseClicked(java.awt.event.MouseEvent evt) {
        // TODO add your handling code here:
        ProcGUI pg = new ProcGUI("cwpa113", pcsMap.get("cwpa113"), this.processorList); //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa114MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa114", pcsMap.get("cwpa114"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpa115MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpa115", pcsMap.get("cwpa115"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void ecsb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecsb601", pcsMap.get("ecsb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void ecsb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecsb602", pcsMap.get("ecsb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void drpsb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("drpsb601", pcsMap.get("drpsb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void drpsb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("drpsb602", pcsMap.get("drpsb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void fdpsb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("fdpsb601", pcsMap.get("fdpsb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void fdpsb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("fdpsb602", pcsMap.get("fdpsb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sdpsb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sdpsb601", pcsMap.get("sdpsb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sdpsb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sdpsb602", pcsMap.get("sdpsb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void syncsrvb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("syncsrvb601", pcsMap.get("syncsrvb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void syncsrvb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("syncsrvb602", pcsMap.get("syncsrvb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppb601", pcsMap.get("mcppb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppb602", pcsMap.get("mcppb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppb603MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppb603", pcsMap.get("mcppb603"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void mcppb604MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppb604", pcsMap.get("mcppb604"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void pppb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("pppb602", pcsMap.get("pppb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sppb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sppb601", pcsMap.get("sppb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void sppb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sppb602", pcsMap.get("sppb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb601MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb601", pcsMap.get("cwpb601"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb602MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb602", pcsMap.get("cwpb602"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb603MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb603", pcsMap.get("cwpb603"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb604MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb604", pcsMap.get("cwpb604"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb605MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb605", pcsMap.get("cwpb605"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb606MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb606", pcsMap.get("cwpb606"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb607MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb607", pcsMap.get("cwpb607"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb608MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb608", pcsMap.get("cwpb608"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb609MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb609", pcsMap.get("cwpb609"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb610MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb610", pcsMap.get("cwpb610"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb611MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb611", pcsMap.get("cwpb611"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb612MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb612", pcsMap.get("cwpb612"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb613MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb613", pcsMap.get("cwpb613"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpb614MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpb614", pcsMap.get("cwpb614"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void ecssims301MouseClicked(java.awt.event.MouseEvent evt) {
        // TODO add your handling code here:
        ProcGUI pg = new ProcGUI("ecssims301", pcsMap.get("ecssims301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void ecssims302MouseClicked(java.awt.event.MouseEvent evt) {
        // TODO add your handling code here:
        ProcGUI pg = new ProcGUI("ecssims302", pcsMap.get("ecssims302"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void supss301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("supss301", pcsMap.get("supss301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void oiiflc1MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("oiiflc1", pcsMap.get("oiiflc1"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpt301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpt301", pcsMap.get("cwpt301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }

    private void cwpt302MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpt302", pcsMap.get("cwpt302"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void cwpt303MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpt303", pcsMap.get("cwpt303"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void cwpt304MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpt304", pcsMap.get("cwpt304"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void cwpt305MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("cwpt305", pcsMap.get("cwpt305"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void ecst301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecst301", pcsMap.get("ecst301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void drpst301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("drpst301", pcsMap.get("drpst301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void fdpst301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("fdpst301", pcsMap.get("fdpst301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void sdpst301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("sdpst301", pcsMap.get("sdpst301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void syncsrvt301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("syncsrvt301", pcsMap.get("syncsrvt301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void mcppt301MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("mcppt301", pcsMap.get("mcppt301"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    
    private void ecssims303MouseClicked(java.awt.event.MouseEvent evt) {
        ProcGUI pg = new ProcGUI("ecssims303", pcsMap.get("ecssims303"), this.processorList);
        pg.setLocationRelativeTo(this);
        pg.setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ChannelA;
    private javax.swing.JPanel ChannelB;
    private javax.swing.JPanel Legend;
    private javax.swing.JPanel MainPanel;
    private javax.swing.JPanel SupportS;
    private javax.swing.JPanel bsPanel;
    private javax.swing.JLabel cwpa101;
    private javax.swing.JPanel cwpa101Panel;
    private javax.swing.JLabel cwpa102;
    private javax.swing.JPanel cwpa102Panel;
    private javax.swing.JLabel cwpa103;
    private javax.swing.JPanel cwpa103Panel;
    private javax.swing.JLabel cwpa104;
    private javax.swing.JPanel cwpa104Panel;
    private javax.swing.JLabel cwpa105;
    private javax.swing.JPanel cwpa105Panel;
    private javax.swing.JLabel cwpa106;
    private javax.swing.JPanel cwpa106Panel;
    private javax.swing.JLabel cwpa107;
    private javax.swing.JPanel cwpa107Panel;
    private javax.swing.JLabel cwpa108;
    private javax.swing.JPanel cwpa108Panel;
    private javax.swing.JLabel cwpa109;
    private javax.swing.JPanel cwpa109Panel;
    private javax.swing.JLabel cwpa110;
    private javax.swing.JPanel cwpa110Panel;
    private javax.swing.JLabel cwpa111;
    private javax.swing.JPanel cwpa111Panel;
    private javax.swing.JLabel cwpa112;
    private javax.swing.JPanel cwpa112Panel;
    private javax.swing.JLabel cwpa113;
    private javax.swing.JPanel cwpa113Panel;
    private javax.swing.JLabel cwpa114;
    private javax.swing.JPanel cwpa114Panel;
    private javax.swing.JLabel cwpa115;
    private javax.swing.JPanel cwpa115Panel;
    private javax.swing.JLabel cwpb601;
    private javax.swing.JPanel cwpb601Panel;
    private javax.swing.JLabel cwpb602;
    private javax.swing.JPanel cwpb602Panel;
    private javax.swing.JLabel cwpb603;
    private javax.swing.JPanel cwpb603Panel;
    private javax.swing.JLabel cwpb604;
    private javax.swing.JPanel cwpb604Panel;
    private javax.swing.JLabel cwpb605;
    private javax.swing.JPanel cwpb605Panel;
    private javax.swing.JLabel cwpb606;
    private javax.swing.JPanel cwpb606Panel;
    private javax.swing.JLabel cwpb607;
    private javax.swing.JPanel cwpb607Panel;
    private javax.swing.JLabel cwpb608;
    private javax.swing.JPanel cwpb608Panel;
    private javax.swing.JLabel cwpb609;
    private javax.swing.JPanel cwpb609Panel;
    private javax.swing.JLabel cwpb610;
    private javax.swing.JPanel cwpb610Panel;
    private javax.swing.JLabel cwpb611;
    private javax.swing.JPanel cwpb611Panel;
    private javax.swing.JLabel cwpb612;
    private javax.swing.JPanel cwpb612Panel;
    private javax.swing.JLabel cwpb613;
    private javax.swing.JPanel cwpb613Panel;
    private javax.swing.JLabel cwpb614;
    private javax.swing.JPanel cwpb614Panel;
    private javax.swing.JLabel cwpt301;
    private javax.swing.JPanel cwpt301Panel;
    private javax.swing.JLabel cwpt302;
    private javax.swing.JPanel cwpt302Panel;
    private javax.swing.JLabel cwpt303;
    private javax.swing.JPanel cwpt303Panel;
    private javax.swing.JLabel cwpt304;
    private javax.swing.JPanel cwpt304Panel;
    private javax.swing.JLabel cwpt305;
    private javax.swing.JPanel cwpt305Panel;
    private javax.swing.JLabel drpsa101;
    private javax.swing.JPanel drpsa101Panel;
    private javax.swing.JLabel drpsa102;
    private javax.swing.JPanel drpsa102Panel;
    private javax.swing.JLabel drpsb601;
    private javax.swing.JPanel drpsb601Panel;
    private javax.swing.JLabel drpsb602;
    private javax.swing.JPanel drpsb602Panel;
    private javax.swing.JLabel drpst301;
    private javax.swing.JPanel drpst301Panel;
    private javax.swing.JLabel ecsa101;
    private javax.swing.JPanel ecsa101Panel;
    private javax.swing.JLabel ecsa102;
    private javax.swing.JPanel ecsa102Panel;
    private javax.swing.JLabel ecsb601;
    private javax.swing.JPanel ecsb601Panel;
    private javax.swing.JLabel ecsb602;
    private javax.swing.JPanel ecsb602Panel;
    private javax.swing.JLabel ecssims301;
    private javax.swing.JLabel ecssims302;
    private javax.swing.JLabel ecssims303;
    private javax.swing.JPanel ecssims303Panel;
    private javax.swing.JLabel ecst301;
    private javax.swing.JPanel ecst301Panel;
    private javax.swing.JLabel fdpsa101;
    private javax.swing.JPanel fdpsa101Panel;
    private javax.swing.JLabel fdpsa102;
    private javax.swing.JPanel fdpsa102Panel;
    private javax.swing.JLabel fdpsb601;
    private javax.swing.JPanel fdpsb601Panel;
    private javax.swing.JLabel fdpsb602;
    private javax.swing.JPanel fdpsb602Panel;
    private javax.swing.JLabel fdpst301;
    private javax.swing.JPanel fdpst301Panel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel mcppa101;
    private javax.swing.JPanel mcppa101Panel;
    private javax.swing.JLabel mcppa102;
    private javax.swing.JPanel mcppa102Panel;
    private javax.swing.JLabel mcppa103;
    private javax.swing.JPanel mcppa103Panel;
    private javax.swing.JLabel mcppa104;
    private javax.swing.JPanel mcppa104Panel;
    private javax.swing.JLabel mcppb601;
    private javax.swing.JPanel mcppb601Panel;
    private javax.swing.JLabel mcppb602;
    private javax.swing.JPanel mcppb602Panel;
    private javax.swing.JLabel mcppb603;
    private javax.swing.JPanel mcppb603Panel;
    private javax.swing.JLabel mcppb604;
    private javax.swing.JPanel mcppb604Panel;
    private javax.swing.JLabel mcppt301;
    private javax.swing.JPanel mcppt301Panel;
    private javax.swing.JPanel motdPanel;
    private javax.swing.JLabel oiiflc1;
    private javax.swing.JPanel oiiflc1Panel;
    private javax.swing.JLabel pppb602;
    private javax.swing.JPanel pppb602Panel;
    private javax.swing.JButton refreshBtn1;
    private javax.swing.JLabel sdpsa101;
    private javax.swing.JPanel sdpsa101Panel;
    private javax.swing.JLabel sdpsa102;
    private javax.swing.JPanel sdpsa102Panel;
    private javax.swing.JLabel sdpsb601;
    private javax.swing.JPanel sdpsb601Panel;
    private javax.swing.JLabel sdpsb602;
    private javax.swing.JPanel sdpsb602Panel;
    private javax.swing.JLabel sdpst301;
    private javax.swing.JPanel sdpst301Panel;
    private javax.swing.JPanel sims301Panel;
    private javax.swing.JPanel sims302Panel;
    private javax.swing.JPanel sncsa101Panel;
    private javax.swing.JPanel sncsa102Panel;
    private javax.swing.JPanel sncsb601Panel;
    private javax.swing.JPanel sncsb602Panel;
    private javax.swing.JLabel sppa101;
    private javax.swing.JPanel sppa101Panel;
    private javax.swing.JLabel sppa102;
    private javax.swing.JPanel sppa102Panel;
    private javax.swing.JLabel sppb601;
    private javax.swing.JPanel sppb601Panel;
    private javax.swing.JLabel sppb602;
    private javax.swing.JPanel sppb602Panel;
    private javax.swing.JLabel supss301;
    private javax.swing.JPanel supssPanel;
    private javax.swing.JLabel syncsrva101;
    private javax.swing.JLabel syncsrva102;
    private javax.swing.JLabel syncsrvb601;
    private javax.swing.JLabel syncsrvb602;
    private javax.swing.JLabel syncsrvt301;
    private javax.swing.JPanel syncsrvt301Panel;
    private javax.swing.JButton toolboxBtn;
    // End of variables declaration//GEN-END:variables
}
