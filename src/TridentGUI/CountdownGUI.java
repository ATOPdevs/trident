/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentGUI;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * GUI for Lab power down countdown timer.
 * @author chrish
 */
public class CountdownGUI extends javax.swing.JFrame implements ActionListener {

    private int seconds;
    private int minutes;
    private boolean done;
    private Timer timer;
    private ArrayList<String> procNames;

    /**
     * Creates new form CountdownGUI
     * @param minutesIn Minutes until shutdown
     * @param secondsIn Seconds until shutdown
     */
    public CountdownGUI(int minutesIn, int secondsIn) {
        initComponents();
        procNames = new ArrayList<String>();
        this.setProcNames();
        done = false;
        seconds = secondsIn;
        minutes = minutesIn;
        this.minutesLbl.setText(minutes + "");
        this.secondsLbl.setText(seconds + "");
        this.setTitle("Countdown");
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addActionListeners();
        this.setVisible(true);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (seconds == 0) {
                    seconds = 59;
                    minutes--;
                    if (minutes < 10) {
                        minutesLbl.setText("0" + minutes);
                    } else {
                        minutesLbl.setText(minutes + "");
                    }
                    if (seconds < 10) {
                        secondsLbl.setText("0" + seconds);
                    } else {
                        secondsLbl.setText(seconds + "");
                    }
                } else {
                    seconds--;
                    if (seconds < 10) {
                        secondsLbl.setText("0" + seconds);
                    } else {
                        secondsLbl.setText(seconds + "");
                    }
                }
                if (minutes < 10) {
                    minutesLbl.setText("0" + minutes);
                } else {
                    minutesLbl.setText(minutes + "");
                }
                if (minutes == 0) {
                    minutesLbl.setForeground(Color.red);
                    secondsLbl.setForeground(Color.red);
                    colon.setForeground(Color.red);
                }
                if (minutes == 0 && seconds == 0) {
                    done = true;
                    shutdown();
                    this.cancel();
                }
            }
        }, 0, 1 * 1000);

    }

    /**
     * Adds ActionListener to Abort button.
     */
    private void addActionListeners() {
        this.abort.addActionListener(this);
        this.abort.setActionCommand("abort");
    }

    /**
     * Handles ActionEvents (Abort and Close).
     * 
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("abort")) {
            timer.cancel();
            abort.setText("Close");
            abort.setActionCommand("close");
        } else if (e.getActionCommand().equals("close")) {
            this.dispose();
        }
    }

    /**
     * Sends shutdown message to all processors.
     */
    private void shutdown() {
        this.setVisible(false);
        CommandGUI cg = new CommandGUI("");
        cg.setVisible(true);
        
        for (int i = 0; i < procNames.size(); i++) {
            cg.getOutput().append("[" + (i + 1) + "/" + procNames.size() + "] Shutting down " + procNames.get(i));
            try {
                Runtime.getRuntime().exec("ssh " + procNames.get(i) + " -l root \"shutdown -h now\"&");
                cg.getOutput().append(" - OK\n");
            } catch (IOException e) {
                cg.getOutput().append(" - FAIL\n");
            }
        }
    }

    /**
     * Creates a list of processor names.
     */
    private void setProcNames() {
        procNames.add("ecsa101");
        procNames.add("ecsa102");
        procNames.add("ecssims301");
        procNames.add("ecsb601");
        procNames.add("ecsb602");
        procNames.add("ecssims302");
        procNames.add("drpsa101");
        procNames.add("drpsa102");
        procNames.add("drpsb601");
        procNames.add("drpsb602");
        procNames.add("sdpsa101");
        procNames.add("sdpsa102");
        procNames.add("sdpsb601");
        procNames.add("sdpsb602");
        procNames.add("fdpsa101");
        procNames.add("fdpsa102");
        procNames.add("fdpsb601");
        procNames.add("fdpsb602");
        procNames.add("frppa101");
        procNames.add("frppa102");
        procNames.add("frppb601");
        procNames.add("frppb602");
        procNames.add("pppa101");
        procNames.add("pppa102");
        procNames.add("pppb601");
        procNames.add("pppb602");
        procNames.add("sppa101");
        procNames.add("sppa102");
        procNames.add("sppb601");
        procNames.add("sppb602");
        procNames.add("mcppa101");
        procNames.add("mcppa102");
        procNames.add("mcppa103");
        procNames.add("mcppa104");
        procNames.add("mcppb601");
        procNames.add("mcppb602");
        procNames.add("mcppb603");
        procNames.add("mcppb604");
        procNames.add("cwpa101");
        procNames.add("cwpa102");
        procNames.add("cwpa103");
        procNames.add("cwpa104");
        procNames.add("cwpa105");
        procNames.add("cwpa106");
        procNames.add("cwpa107");
        procNames.add("cwpa108");
        procNames.add("cwpa109");
        procNames.add("cwpa110");
        procNames.add("cwpa111");
        procNames.add("cwpa112");
        procNames.add("cwpb601");
        procNames.add("cwpb602");
        procNames.add("cwpb603");
        procNames.add("cwpb604");
        procNames.add("cwpb605");
        procNames.add("cwpb606");
        procNames.add("cwpb607");
        procNames.add("cwpb608");
        procNames.add("cwpb609");
        procNames.add("cwpb610");
        procNames.add("cwpb611");
        procNames.add("cwpb612");
        procNames.add("syncsrva101");
        procNames.add("syncsrva102");
        procNames.add("syncsrvb601");
        procNames.add("syncsrvb602");
        //procNames.add("oiiflc1");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        minutesLbl = new javax.swing.JLabel();
        colon = new javax.swing.JLabel();
        secondsLbl = new javax.swing.JLabel();
        abort = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        minutesLbl.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        minutesLbl.setForeground(new java.awt.Color(255, 255, 255));
        minutesLbl.setText("00");

        colon.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        colon.setForeground(new java.awt.Color(255, 255, 255));
        colon.setText(":");

        secondsLbl.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        secondsLbl.setForeground(new java.awt.Color(255, 255, 255));
        secondsLbl.setText("00");

        abort.setBackground(new java.awt.Color(0, 0, 0));
        abort.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        abort.setForeground(new java.awt.Color(102, 102, 102));
        abort.setText("Abort");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(101, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(abort))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(minutesLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(colon)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(secondsLbl)))
                .addGap(95, 95, 95))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(minutesLbl)
                    .addComponent(colon)
                    .addComponent(secondsLbl))
                .addGap(18, 18, 18)
                .addComponent(abort)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton abort;
    private javax.swing.JLabel colon;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel minutesLbl;
    private javax.swing.JLabel secondsLbl;
    // End of variables declaration//GEN-END:variables
}
