/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentGUI;

import Trident.Processor;
import TridentUtil.Command;
import TridentUtil.CommandWorker;
import TridentUtil.PCSHighRes;
import TridentUtil.PCSWorker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * GUI for individual Processors.
 *
 * @author chrish
 */
public class ProcGUI extends javax.swing.JFrame implements ActionListener {

    private String hostname;
    private PCSHighRes pcs1;
    private ArrayList<Processor> processorList;

    /**
     * Creates new form ProcGUI
     *
     * @param hostnameIn Hostname
     * @param pcs1In PCS object
     * @param processorListIn Processor List (For unloading communities)
     */
    public ProcGUI(String hostnameIn, PCSHighRes pcs1In, ArrayList<Processor> processorListIn) {
        hostname = hostnameIn;
        pcs1 = pcs1In;
        processorList = processorListIn;
        initComponents();
        this.setIconImage(new ImageIcon(this.getClass().getResource("/TridentGUI/Resources/powersm.png")).getImage());
        this.setTitle(hostname.toUpperCase());
        addActionListeners();
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        if (hostname.equals("oiiflc1")) {
//            this.cfnoLbl.setVisible(false);
//            this.roleLbl.setVisible(false);
//            this.modeLbl.setVisible(false);
            this.cfno.setVisible(false);
            this.role.setVisible(false);
            this.mode.setVisible(false);
            this.cfnoLbl.setText(" ");
            this.roleLbl.setText(" ");
            this.modeLbl.setText(" ");
            this.getCfno(hostname);
        } else if (this.getProc().getCfno().equals("") || !this.getProc().getStatus().equals("loaded_up")) {
            this.cfnoLbl.setText("Last known CFNO:");
            this.roleLbl.setText("Last known Role:");
            this.modeLbl.setText("Last known Mode:");
            this.getCfno(hostname);
        } else {
            this.cfnoLbl.setText("CFNO:");
            this.roleLbl.setText("Role:");
            this.modeLbl.setText("Mode:");
            this.getCfno(hostname);
        }
        this.getStatus();
        this.checkButtons();
    }

    /**
     * Adds ActionListeners to buttons.
     */
    private void addActionListeners() {
        this.refreshBtn.addActionListener(this);
        this.refreshBtn.setActionCommand("refresh");
        this.closeBtn.addActionListener(this);
        this.closeBtn.setActionCommand("close");
        this.xtermBtn.addActionListener(this);
        this.xtermBtn.setActionCommand("xterm");
        this.unloadBtn.addActionListener(this);
        this.unloadBtn.setActionCommand("unload");
        this.unloadCBtn.addActionListener(this);
        this.unloadCBtn.setActionCommand("unloadc");
        this.shutdownBtn.addActionListener(this);
        this.shutdownBtn.setActionCommand("shutdown");
        this.rebootBtn.addActionListener(this);
        this.rebootBtn.setActionCommand("reboot");
        this.broadcast.addActionListener(this);
        this.broadcast.setActionCommand("broadcast");
        this.clone.addActionListener(this);
        this.clone.setActionCommand("clone");
    }

    /**
     * Handles button actions.
     *
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("refresh")) {
            this.getStatus();
        } else if (e.getActionCommand().equals("close")) {
            this.dispose();
        } else if (e.getActionCommand().equals("reboot")) {
            int j = JOptionPane.showConfirmDialog(this,
                    "Are you sure you want to reboot " + hostname.toUpperCase() + "?",
                    "Reboot " + hostname.toUpperCase(),
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (j == JOptionPane.YES_OPTION) {
                CommandWorker cw = new CommandWorker();
                cw.setRunnable(new Command("ssh " + hostname + " -l root \"shutdown -r now\"&", new CommandGUI(hostname.toUpperCase()), "Reboot " + hostname.toUpperCase()));
                cw.execute();
            }
        } else if (e.getActionCommand().equals("unload")) {
            if (!hostname.startsWith("ecssim")) {
                int j = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to unload " + hostname.toUpperCase() + "?",
                        "Unload " + hostname.toUpperCase(),
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (j == JOptionPane.YES_OPTION) {
                    String cmdNode = "ssh" + " " + hostname + " " + "-l atop \"logicals -l | grep atc_node\"";
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmdNode);
                    try {
                        Process shell = pb.start();
                        BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                        String s;
                        while ((s = stdInput.readLine()) != null) {
                            if (s.startsWith("atc_node")) {
                                CommandWorker cw = new CommandWorker();
                                cw.setRunnable(new Command("ssh " + hostname + " -l atop \"/ocean21/bootstrap/unload -force -node " + s.split("=")[1] + " -force -noconfirm -dae\"", new CommandGUI(hostname), "Unload " + hostname));
                                cw.execute();
                            }
                        }
                    } catch (IOException ex) {
                    }
                }
            } else if (hostname.startsWith("ecssim")) {
                int j = JOptionPane.showConfirmDialog(this,
                        "Are you sure you want to unload " + hostname.toUpperCase() + "?",
                        "Unload " + hostname.toUpperCase(),
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (j == JOptionPane.YES_OPTION) {
                    String cmdNode = "ssh" + " " + hostname + " " + "-l atop \"ps -ef |grep interface_connector.exe | grep -v grep\"";
                    ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmdNode);
                    try {
                        Process shell = pb.start();
                        BufferedReader stdInput = new BufferedReader(new InputStreamReader(shell.getInputStream()));
                        String s;
                        while ((s = stdInput.readLine()) != null) {
                            if (!s.startsWith("")) {
                                CommandWorker cw = new CommandWorker();
                                cw.setRunnable(new Command("ssh " + hostname + " -l atop \"kill -9 interface_connector.exe", new CommandGUI(hostname), "Unload " + hostname));
                                cw.execute();
                            }
                        }
                    } catch (IOException ex) {
                    }
                }
            }
        } else if (e.getActionCommand().equals("unloadc")) {
            ArrayList<String> list = new ArrayList<String>();
            String str = "";
            //gets channel
            String channelLetter = hostname.substring(hostname.length() - 4, hostname.length() - 3);
            for (int i = 0; i < processorList.size(); i++) {
                if (this.getProc().getCfno().equals(processorList.get(i).getCfno()) && (processorList.get(i).getHostname().substring(processorList.get(i).getHostname().length() - 4, (processorList.get(i).getHostname().length()) - 3).equals(channelLetter))) {
                    list.add(processorList.get(i).getHostname());
                }
            }
            Collections.sort(list);
            for (int i = 0; i < list.size(); i++) {
                str = str + list.get(i) + "\n";
            }
            int j = JOptionPane.showConfirmDialog(this,
                    "The following processors will be unloaded:\n" + str + "\nContinue?",
                    "Unload Community",
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (j == JOptionPane.YES_OPTION) {
                CommandWorker cw = new CommandWorker();
                cw.setRunnable(new Command("ssh " + hostname + " -l atop \"/ocean21/bootstrap/unload -force -noconfirm -dae\"", new CommandGUI(hostname), "Unload Community"));
                cw.execute();
            }

        } else if (e.getActionCommand().equals("shutdown")) {
            int j = JOptionPane.showConfirmDialog(this,
                    "Are you sure you want to shutdown " + hostname.toUpperCase() + "?",
                    "Shut Down " + hostname.toUpperCase(),
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (j == JOptionPane.YES_OPTION) {
                CommandWorker cw = new CommandWorker();
                cw.setRunnable(new Command("ssh " + hostname + " -l root \"shutdown -h now\"&", new CommandGUI(hostname.toUpperCase()), "Shut Down " + hostname.toUpperCase()));
                cw.execute();
            }
        } else if (e.getActionCommand().equals("xterm")) {
            String rshssh = "";
            if (hostname.equals("oiiflc1")) {
                rshssh = "ssh ";
            } else {
                rshssh = "ssh ";
            }
            ProcessBuilder pb = new ProcessBuilder("ksh", "-c", "xterm -title '" + hostname + "' -bg gray65 -fg black -sb -e \'" + rshssh + hostname + " -l atop\'");
            try {
                pb.start();
            } catch (IOException ex) {
                System.out.println(ex);
            }
        } else if (e.getActionCommand().equals("broadcast")) {
            //ProcessBuilder pb = new ProcessBuilder("ksh", "-c", "rsh " + hostname + " -l atop \"wall "+message+"\"");
            //try {
            //    pb.start();
            //} catch (IOException ex) {
            //    System.out.println(ex);
            //}
        }
    }

    /**
     * Checks which buttons should be enabled/disabled.
     */
    private void checkButtons() {
        if (!this.getProc().getStatus().equals("loaded_up")) {
            this.unloadBtn.setEnabled(false);
            this.unloadCBtn.setEnabled(false);
        } else {
            if (!hostname.startsWith("ecssims")) {
                this.unloadBtn.setEnabled(true);
                this.unloadCBtn.setEnabled(true);
            } else {
                this.unloadBtn.setEnabled(true);
                this.unloadCBtn.setEnabled(false);
            }
        }
        if (this.getProc().getStatus().equals("down")) {
            this.xtermBtn.setEnabled(false);
            this.rebootBtn.setEnabled(false);
            this.shutdownBtn.setEnabled(false);
            this.broadcast.setEnabled(false);
            this.clone.setEnabled(false);
        } else {
            this.xtermBtn.setEnabled(true);
            this.rebootBtn.setEnabled(true);
            this.shutdownBtn.setEnabled(true);
            this.broadcast.setEnabled(false);
            this.clone.setEnabled(false);
        }
    }

    /**
     * Gets the processor's status.
     */
    private void getStatus() {
        if (pcs1.getProc().isDistrib()) {
            this.statusLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/dist150px.png")));
            this.distlabel.setText("DISTRIBUTING BY USER: " + pcs1.getProc().getDistUser().toUpperCase());
            this.distlabel.setVisible(true);
        } else {
            this.distlabel.setVisible(false);
            PCSHighRes pcs = new PCSHighRes(this.hostname, this.statusLbl, true, true, true, true, true, true);
            pcs.dontGetCfno();
            pcs.setProc(pcs1.getProc());
            PCSWorker pcsw = new PCSWorker();
            pcsw.setRunnable(pcs);
            pcsw.execute();
        }
    }

    /**
     * Gets the Processor's CFNO.
     *
     * @param hostname Hostname
     */
    private void getCfno(String hostname) {
        String cmdCfno = "ssh" + " " + hostname + " " + "-l atop \"logicals -l | grep atc_load_platform; logicals -l | grep cfno; logicals -l | grep atc_node; logicals -l | grep atc_mode\"";
        ProcessBuilder pb = new ProcessBuilder("ksh", "-c", cmdCfno);
        try {
            Process shell = pb.start();
            BufferedReader stdInput4 = new BufferedReader(new InputStreamReader(shell.getInputStream()));
            String s;
            while ((s = stdInput4.readLine()) != null) {;
                if (s.startsWith("atc_cfno")) {
                    this.cfno.setText(s.split("=")[1]);
                } else if (s.startsWith("atc_node")) {
                    this.role.setText(s.split("=")[1]);
                } else if (s.startsWith("atc_mode")) {
                    this.mode.setText(s.split("=")[1]);
                }
            }
        } catch (IOException e) {
            this.cfno.setText("");
            this.cfno.setVisible(false);
            this.cfnoLbl.setVisible(false);
        }
        if (cfno.getText().equals("") || cfno.getText().equalsIgnoreCase("cfno")) {
            this.cfno.setText("");
            this.cfno.setVisible(false);
            this.cfnoLbl.setVisible(false);
        }
        if (role.getText().equals("") || role.getText().equalsIgnoreCase("cfno")) {
            this.role.setText("");
            this.role.setVisible(false);
            this.roleLbl.setVisible(false);
        }
    }

    /**
     * Returns the Processor object.
     *
     * @return Processor object.
     */
    private Processor getProc() {
        for (int i = 0; i < processorList.size(); i++) {
            if (processorList.get(i).getHostname().equals(this.hostname)) {
                return processorList.get(i);
            }
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        cfno = new javax.swing.JLabel();
        cfnoLbl = new javax.swing.JLabel();
        roleLbl = new javax.swing.JLabel();
        modeLbl = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        statusLbl = new javax.swing.JLabel();
        mode = new javax.swing.JLabel();
        role = new javax.swing.JLabel();
        xtermBtn = new javax.swing.JButton();
        refreshBtn = new javax.swing.JButton();
        broadcast = new javax.swing.JButton();
        rebootBtn = new javax.swing.JButton();
        shutdownBtn = new javax.swing.JButton();
        clone = new javax.swing.JButton();
        unloadCBtn = new javax.swing.JButton();
        unloadBtn = new javax.swing.JButton();
        closeBtn = new javax.swing.JButton();
        distlabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        cfno.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        cfno.setForeground(new java.awt.Color(255, 255, 255));
        cfno.setText("Unknown");

        cfnoLbl.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        cfnoLbl.setForeground(new java.awt.Color(255, 255, 255));
        cfnoLbl.setText("CFNO:");

        roleLbl.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        roleLbl.setForeground(new java.awt.Color(255, 255, 255));
        roleLbl.setText("Role:");

        modeLbl.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        modeLbl.setForeground(new java.awt.Color(255, 255, 255));
        modeLbl.setText("Mode:");

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 24), new java.awt.Color(255, 255, 255))); // NOI18N

        statusLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/TridentGUI/Resources/blank80px.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mode.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        mode.setForeground(new java.awt.Color(255, 255, 255));
        mode.setText("Unknown");

        role.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        role.setForeground(new java.awt.Color(255, 255, 255));
        role.setText("Unknown");

        xtermBtn.setBackground(new java.awt.Color(51, 51, 51));
        xtermBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        xtermBtn.setForeground(new java.awt.Color(255, 255, 255));
        xtermBtn.setText("xterm");

        refreshBtn.setBackground(new java.awt.Color(51, 51, 51));
        refreshBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        refreshBtn.setForeground(new java.awt.Color(255, 255, 255));
        refreshBtn.setText("Refresh Status");

        broadcast.setBackground(new java.awt.Color(51, 51, 51));
        broadcast.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        broadcast.setForeground(new java.awt.Color(255, 255, 255));
        broadcast.setText("Broadcast");

        rebootBtn.setBackground(new java.awt.Color(51, 51, 51));
        rebootBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        rebootBtn.setForeground(new java.awt.Color(255, 255, 255));
        rebootBtn.setText("Reboot");

        shutdownBtn.setBackground(new java.awt.Color(51, 51, 51));
        shutdownBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        shutdownBtn.setForeground(new java.awt.Color(255, 255, 255));
        shutdownBtn.setText("Shut Down");

        clone.setBackground(new java.awt.Color(51, 51, 51));
        clone.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        clone.setForeground(new java.awt.Color(255, 255, 255));
        clone.setText("Disk Clone");

        unloadCBtn.setBackground(new java.awt.Color(51, 51, 51));
        unloadCBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        unloadCBtn.setForeground(new java.awt.Color(255, 255, 255));
        unloadCBtn.setText("Unload Community");

        unloadBtn.setBackground(new java.awt.Color(51, 51, 51));
        unloadBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        unloadBtn.setForeground(new java.awt.Color(255, 255, 255));
        unloadBtn.setText("Unload");

        closeBtn.setBackground(new java.awt.Color(51, 51, 51));
        closeBtn.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        closeBtn.setForeground(new java.awt.Color(255, 255, 255));
        closeBtn.setText("Close");

        distlabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        distlabel.setForeground(new java.awt.Color(255, 255, 255));
        distlabel.setText("DISTRIBUTING BY USER: ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(cfnoLbl)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cfno))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(roleLbl)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(role)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(closeBtn))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(modeLbl)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mode)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(rebootBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(refreshBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(xtermBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(broadcast, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(unloadCBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(clone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(unloadBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(shutdownBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(distlabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(closeBtn)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cfnoLbl)
                            .addComponent(cfno))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(roleLbl)
                            .addComponent(role))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modeLbl)
                    .addComponent(mode))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(distlabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(xtermBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(unloadBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(refreshBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(unloadCBtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(broadcast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(clone))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rebootBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(shutdownBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * @param args the command line arguments
     *
     * public static void main(String args[]) { /* Set the Nimbus look and feel
     */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
     * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         
     try {
     for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
     if ("Nimbus".equals(info.getName())) {
     javax.swing.UIManager.setLookAndFeel(info.getClassName());
     break;
     }
     }
     } catch (ClassNotFoundException ex) {
     java.util.logging.Logger.getLogger(ProcGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (InstantiationException ex) {
     java.util.logging.Logger.getLogger(ProcGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (IllegalAccessException ex) {
     java.util.logging.Logger.getLogger(ProcGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     } catch (javax.swing.UnsupportedLookAndFeelException ex) {
     java.util.logging.Logger.getLogger(ProcGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
     }
     //</editor-fold>

     /* Create and display the form 
     java.awt.EventQueue.invokeLater(new Runnable() {
     public void run() {
     new ProcGUI().setVisible(true);
     }
     });
     }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton broadcast;
    private javax.swing.JLabel cfno;
    private javax.swing.JLabel cfnoLbl;
    private javax.swing.JButton clone;
    private javax.swing.JButton closeBtn;
    private javax.swing.JLabel distlabel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel mode;
    private javax.swing.JLabel modeLbl;
    private javax.swing.JButton rebootBtn;
    private javax.swing.JButton refreshBtn;
    private javax.swing.JLabel role;
    private javax.swing.JLabel roleLbl;
    private javax.swing.JButton shutdownBtn;
    private javax.swing.JLabel statusLbl;
    private javax.swing.JButton unloadBtn;
    private javax.swing.JButton unloadCBtn;
    private javax.swing.JButton xtermBtn;
    // End of variables declaration//GEN-END:variables
}
