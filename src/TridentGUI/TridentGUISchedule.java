/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TridentGUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * GUI for schedule viewing.
 * 
 * @author chrish
 */
public class TridentGUISchedule extends javax.swing.JFrame {

    /**
     * Creates new form TridentLiteGUIHighRes.
     */
    public TridentGUISchedule() {
        initComponents();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                calendarFunctions();
            }
        }, 0, 60 * 10000);
    }

    /**
     * Fixes time formats.
     * 
     * @param timeIn Time (Unix Time)
     * @return Formatted time
     */
    private String fixtime(int timeIn) {
        if (timeIn % 60 == 0) {
            int temptime = timeIn / 60;
            if (temptime < 10) {
                return "0" + temptime + ":00";
            } else {
                return temptime + ":00";
            }
        } else {
            int temptime = (int) Math.floor(timeIn / 60);
            if (temptime < 10) {
                return "0" + temptime + ":30";
            } else {
                return temptime + ":30";
            }
        }
    }

    /**
     * Functions for accessing the web calendar.
     */
    private void calendarFunctions() {
        try {
            Calendar cal = Calendar.getInstance();
            Date d = cal.getTime();
            long offset = cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET);

            DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.MEDIUM);
            //int currtime = (int) (System.currentTimeMillis() / 1000L);
            int currtime = (int) ((cal.getTimeInMillis() + offset) / 1000L);
            int today = currtime - (currtime % 86400);
            int tomorrow = today + 86400;
            int dayaftertomorrow = tomorrow + 86400;

            todaytexa.setText("");
            todaytextb.setText("");
            singletext.setText("");
            tomorrowtext.setText("");
            colossustext.setText("");
            
            sched(today, tomorrow, true);
            sched(tomorrow, dayaftertomorrow, false);
            colossuscheck(currtime, today);
            update.setText("Last Updated: " + df.format(cal.getTime()));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Checks for automated distributions.
     * 
     * @param currtimeIn Current Time (Unix Time)
     * @param todayIn Current Day (Unix Time)
     */
    private void colossuscheck(int currtimeIn, int todayIn) {
        int lookahead = currtimeIn + 14760;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            Connection connect = DriverManager.getConnection("jdbc:mariadb://172.26.159.70/phpScheduleIt?user=root&password=atop0021");
            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from phpScheduleIt.colossus where start_date>" + todayIn);
            while (resultSet.next()) {
                String channel = "";
                String user = "";
                if (resultSet.getInt("start_date") + resultSet.getInt("starttime") * 60 > currtimeIn
                        && resultSet.getInt("start_date") + resultSet.getInt("starttime") * 60 < lookahead) {
                    Statement statement2 = connect.createStatement();
                    ResultSet resultSet2 = statement2.executeQuery("SELECT login.fname, login.lname FROM login INNER JOIN reservation_users ON login.memberid = reservation_users.memberid WHERE reservation_users.resid = '" + resultSet.getString("resid") + "'");
                    while (resultSet2.next()) {
                        user = resultSet2.getString("fname") + " " + resultSet2.getString("lname");
                    }
                    Statement statement3 = connect.createStatement();
                    ResultSet resultSet3 = statement3.executeQuery("SELECT resources.name FROM resources INNER JOIN reservations ON resources.machid = reservations.machid WHERE reservations.resid = '" + resultSet.getString("resid") + "'");
                    while (resultSet3.next()) {
                        channel = resultSet3.getString("name");
                    }
                    colossustext.append("<" + fixtime(resultSet.getInt("starttime")) + "> " + channel + " - " + user + "\n");
                    System.out.println("<" + fixtime(resultSet.getInt("starttime")) + "> " + channel + " - " + user + "\n");
                    if(resultSet.getInt("start_date") + resultSet.getInt("starttime") * 60 - currtimeIn <= 10 &&
                            resultSet.getInt("start_date") + resultSet.getInt("starttime") * 60 - currtimeIn > 1) {
                        ColossusWarningGUI warn = new ColossusWarningGUI(user, channel, fixtime(resultSet.getInt("starttime")));
                        warn.setLocationRelativeTo(this);
                        warn.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
                        warn.setVisible(true);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Gets reservations between two days.
     * 
     * @param day1 First Day
     * @param day2 Second Day
     * @param todayflag If it should look for today.
     */
    private void sched(int day1, int day2, boolean todayflag) {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            Connection connect = DriverManager.getConnection("jdbc:mariadb://172.26.159.70/phpScheduleIt?user=root&password=atop0021");
            Statement statement = connect.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from phpScheduleIt.reservations where start_date>" + day1 + " AND end_date<" + day2 + " and is_blackout=0 order by starttime");
            
            while (resultSet.next()) {
                String resid = resultSet.getString("resid");
                if (!todayflag) {
                }
                String user = "";
                String channel = "";
                Statement statement2 = connect.createStatement();
                ResultSet resultSet2 = statement2.executeQuery("SELECT login.fname, login.lname FROM login INNER JOIN reservation_users ON login.memberid = reservation_users.memberid WHERE reservation_users.resid = '" + resid + "'");
                while (resultSet2.next()) {
                    user = resultSet2.getString("fname") + " " + resultSet2.getString("lname");
                }
                Statement statement3 = connect.createStatement();
                ResultSet resultSet3 = statement3.executeQuery("SELECT resources.name FROM resources INNER JOIN reservations ON resources.machid = reservations.machid WHERE reservations.resid = '" + resid + "'");
                while (resultSet3.next()) {
                    channel = resultSet3.getString("name");
                }
                String starttime = fixtime(resultSet.getInt("starttime"));
                String endtime = fixtime(resultSet.getInt("endtime"));
                if (todayflag) {
                    if (channel.startsWith("Channel A") || channel.startsWith("Full")) {
                        if (channel.startsWith("Channel A")) {
                            todaytexa.append("<" + starttime + "-" + endtime + "> " + channel.replace("Channel ", "") + " - " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + ">\n");
                        } else {
                            todaytexa.append("<" + starttime + "-" + endtime + "> A - " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + ">\n");
                        }
                    }
                    if (channel.startsWith("Channel B") || channel.startsWith("Full")) {
                        if (channel.startsWith("Channel B")) {
                            todaytextb.append("<" + starttime + "-" + endtime + "> " + channel.replace("Channel ", "") + " - " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + ">\n");
                        } else {
                            todaytextb.append("<" + starttime + "-" + endtime + "> B - " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + ">\n");
                        }
                    }
                    if (channel.startsWith("Single")){
                        singletext.append("<" + starttime + "-" + endtime + "> " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + "- " + resultSet.getString("summary") + ">\n");
                    }
                } else {
                    tomorrowtext.append("<" + starttime + "-" + endtime + "> " + channel.toUpperCase() + " - " + user + " <" + resultSet.getString("task").replace("&amp;", "&").toUpperCase() + "- " + resultSet.getString("summary") + ">\n");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainpanel = new javax.swing.JPanel();
        tomorrowsched = new javax.swing.JPanel();
        tomorrowpane = new javax.swing.JScrollPane();
        tomorrowtext = new javax.swing.JTextArea();
        upcomingcolossus = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        colossustext = new javax.swing.JTextArea();
        todaysched = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        todaytexa = new javax.swing.JTextArea();
        update = new javax.swing.JLabel();
        todaysched1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        todaytextb = new javax.swing.JTextArea();
        singlepanel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        singletext = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));

        mainpanel.setBackground(new java.awt.Color(0, 0, 0));

        tomorrowsched.setBackground(new java.awt.Color(0, 0, 0));
        tomorrowsched.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TOMORROW'S SCHEDULE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        tomorrowtext.setEditable(false);
        tomorrowtext.setBackground(new java.awt.Color(0, 0, 0));
        tomorrowtext.setColumns(20);
        tomorrowtext.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        tomorrowtext.setForeground(new java.awt.Color(102, 153, 255));
        tomorrowtext.setRows(5);
        tomorrowtext.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tomorrowtext.setFocusable(false);
        tomorrowpane.setViewportView(tomorrowtext);

        javax.swing.GroupLayout tomorrowschedLayout = new javax.swing.GroupLayout(tomorrowsched);
        tomorrowsched.setLayout(tomorrowschedLayout);
        tomorrowschedLayout.setHorizontalGroup(
            tomorrowschedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, tomorrowschedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tomorrowpane)
                .addContainerGap())
        );
        tomorrowschedLayout.setVerticalGroup(
            tomorrowschedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tomorrowschedLayout.createSequentialGroup()
                .addComponent(tomorrowpane, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                .addContainerGap())
        );

        upcomingcolossus.setBackground(new java.awt.Color(0, 0, 0));
        upcomingcolossus.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "UPCOMING COLOSSUS RUNS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        colossustext.setEditable(false);
        colossustext.setBackground(new java.awt.Color(0, 0, 0));
        colossustext.setColumns(20);
        colossustext.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        colossustext.setForeground(new java.awt.Color(102, 153, 255));
        colossustext.setRows(5);
        colossustext.setFocusable(false);
        jScrollPane2.setViewportView(colossustext);

        javax.swing.GroupLayout upcomingcolossusLayout = new javax.swing.GroupLayout(upcomingcolossus);
        upcomingcolossus.setLayout(upcomingcolossusLayout);
        upcomingcolossusLayout.setHorizontalGroup(
            upcomingcolossusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(upcomingcolossusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        upcomingcolossusLayout.setVerticalGroup(
            upcomingcolossusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(upcomingcolossusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        todaysched.setBackground(new java.awt.Color(0, 0, 0));
        todaysched.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CHANNEL A SCHEDULE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        todaytexa.setEditable(false);
        todaytexa.setBackground(new java.awt.Color(0, 0, 0));
        todaytexa.setColumns(20);
        todaytexa.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        todaytexa.setForeground(new java.awt.Color(255, 255, 255));
        todaytexa.setRows(5);
        todaytexa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        todaytexa.setFocusable(false);
        jScrollPane1.setViewportView(todaytexa);

        javax.swing.GroupLayout todayschedLayout = new javax.swing.GroupLayout(todaysched);
        todaysched.setLayout(todayschedLayout);
        todayschedLayout.setHorizontalGroup(
            todayschedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, todayschedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
                .addContainerGap())
        );
        todayschedLayout.setVerticalGroup(
            todayschedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(todayschedLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addContainerGap())
        );

        update.setFont(new java.awt.Font("DejaVu Sans", 1, 18)); // NOI18N
        update.setForeground(new java.awt.Color(255, 255, 255));
        update.setText("Last Updated: ");

        todaysched1.setBackground(new java.awt.Color(0, 0, 0));
        todaysched1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CHANNEL B SCHEDULE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        todaytextb.setEditable(false);
        todaytextb.setBackground(new java.awt.Color(0, 0, 0));
        todaytextb.setColumns(20);
        todaytextb.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        todaytextb.setForeground(new java.awt.Color(255, 255, 255));
        todaytextb.setRows(5);
        todaytextb.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        todaytextb.setFocusable(false);
        jScrollPane3.setViewportView(todaytextb);

        javax.swing.GroupLayout todaysched1Layout = new javax.swing.GroupLayout(todaysched1);
        todaysched1.setLayout(todaysched1Layout);
        todaysched1Layout.setHorizontalGroup(
            todaysched1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, todaysched1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
                .addContainerGap())
        );
        todaysched1Layout.setVerticalGroup(
            todaysched1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(todaysched1Layout.createSequentialGroup()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );

        singlepanel.setBackground(new java.awt.Color(0, 0, 0));
        singlepanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SINGLE BOX SCHEDULE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 3, 36), new java.awt.Color(51, 153, 255))); // NOI18N

        singletext.setEditable(false);
        singletext.setBackground(new java.awt.Color(0, 0, 0));
        singletext.setColumns(20);
        singletext.setFont(new java.awt.Font("DejaVu Sans", 1, 24)); // NOI18N
        singletext.setForeground(new java.awt.Color(255, 255, 255));
        singletext.setRows(5);
        singletext.setFocusable(false);
        jScrollPane4.setViewportView(singletext);

        javax.swing.GroupLayout singlepanelLayout = new javax.swing.GroupLayout(singlepanel);
        singlepanel.setLayout(singlepanelLayout);
        singlepanelLayout.setHorizontalGroup(
            singlepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(singlepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        singlepanelLayout.setVerticalGroup(
            singlepanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(singlepanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );

        javax.swing.GroupLayout mainpanelLayout = new javax.swing.GroupLayout(mainpanel);
        mainpanel.setLayout(mainpanelLayout);
        mainpanelLayout.setHorizontalGroup(
            mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainpanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(update)
                    .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(mainpanelLayout.createSequentialGroup()
                            .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(todaysched, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(singlepanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(todaysched1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(upcomingcolossus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(tomorrowsched, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        mainpanelLayout.setVerticalGroup(
            mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainpanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(todaysched, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(todaysched1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(mainpanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(upcomingcolossus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(singlepanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tomorrowsched, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(update)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainpanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea colossustext;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel mainpanel;
    private javax.swing.JPanel singlepanel;
    private javax.swing.JTextArea singletext;
    private javax.swing.JPanel todaysched;
    private javax.swing.JPanel todaysched1;
    private javax.swing.JTextArea todaytexa;
    private javax.swing.JTextArea todaytextb;
    private javax.swing.JScrollPane tomorrowpane;
    private javax.swing.JPanel tomorrowsched;
    private javax.swing.JTextArea tomorrowtext;
    private javax.swing.JPanel upcomingcolossus;
    private javax.swing.JLabel update;
    // End of variables declaration//GEN-END:variables
}
