/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Trident;

/**
 * Processor Object.
 * 
 * @author chrish
 */
public class Processor {
    
    private String hostname;
    private String status;
    private String cfno;
    private boolean isDistrib;
    private String distUser;
    
    /**
     * Constructor.
     * @param hostnameIn
     */
    public Processor(String hostnameIn) {
        hostname = hostnameIn;
        status = "";
        cfno = "";
        isDistrib=false;
        distUser = "";
    }
    
    /**
     * Returns the hostname.
     * @return Hostname
     */
    public String getHostname() {
        return hostname;
    }
    
    /**
     * Returns the status.
     * @return Status
     */
    public String getStatus() {
        return status;
    }
    
    /**
     * Returns the CFNO.
     * @return CFNO
     */
    public String getCfno() {
        return cfno;
    }
    
    /**
     * Returns the distributing user.
     * @return Distributing user
     */
    public String getDistUser() {
        return distUser;
    }
    
    /**
     * Returns boolean based on if the processor is being distributed to.
     * @return True/False
     */
    public boolean isDistrib() {
        return isDistrib;
    }
    
    /**
     * Sets the processor's CFNO.
     * @param cfnoIn
     */
    public void setCfno(String cfnoIn) {
        cfno = cfnoIn;
    }
    
    /**
     * Sets the processor's status.
     * @param statusIn
     */
    public void setStatus(String statusIn) {
        status = statusIn;
    }
    
    /**
     * Sets the processor's hostname.
     * @param hostnameIn
     */
    public void setHostname(String hostnameIn) {
        hostname = hostnameIn;
    }
    
    /**
     * Sets whether or not the processor is being distributed to.
     * @param distIn
     */
    public void setDistrib(boolean distIn) {
        isDistrib = distIn;
    }
    
    /**
     * Sets the distribution user.
     * @param distUserIn
     */
    public void setDistUser(String distUserIn) {
        distUser = distUserIn;
    }
}
