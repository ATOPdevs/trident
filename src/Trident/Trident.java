package Trident;

import TridentGUI.TridentGUIMonitor;
import TridentUtil.TridentConstants;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 * Trident Application. Monitors the status of processors in the OIIF.
 *
 * @author chrish
 */
public class Trident extends JPanel {

    /**
     * Constructor.
     */
    public Trident() {
        this.run();
    }

    /**
     * Run method.
     */
    private void run() {
        TridentGUIMonitor tg = new TridentGUIMonitor();
        tg.setTitle("Trident " + TridentConstants.VERSION + " - Lab Monitor");
        tg.setVisible(true);
        tg.setIconImage(new ImageIcon(this.getClass().getResource("/TridentGUI/Resources/powersm.png")).getImage());
        tg.setVisible(true);
        tg.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Main method.
     *
     * @param args
     */
    public static void main(String[] args) {
        Trident t = new Trident();
    }
}
